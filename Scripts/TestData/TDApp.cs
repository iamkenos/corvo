﻿using Corvo.Common;

namespace Corvo.Scripts.TestData
{
    class TDApp
    {
        public static string GetAPPURL
        {
            get
            {
                return ExcelAdapter.ReadData("APP", "APPURL");
            }
        }

        public static string GetAPPRegistrationURL
        {
            get
            {
                return ExcelAdapter.ReadData("APP", "APPRegistrationURL");
            }
        }

        public static string GetAPPSignOnURL
        {
            get
            {
                return ExcelAdapter.ReadData("APP", "APPSignOnURL");
            }
        }

        public static string GetAPPFlightFinderURL
        {
            get
            {
                return ExcelAdapter.ReadData("APP", "APPFlightFinderURL");
            }
        }

        public static string GetAPPUsername
        {
            get
            {
                return ExcelAdapter.ReadData("APP", "APPUsername");
            }
        }

        public static string GetAPPPassword
        {
            get
            {
                return ExcelAdapter.ReadData("APP", "APPPassword");
            }
        }
    }
}
