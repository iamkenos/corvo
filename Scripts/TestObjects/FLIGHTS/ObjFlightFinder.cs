﻿using Corvo.Common;
using Corvo.Scripts.TestData;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Corvo.Scripts.TestObjects.FLIGHTS
{
    class ObjFlightFinder
    {
        public ReadOnlyCollection<IWebElement> GetElemTripTypeRadio
        {
            get
            {
                return WebDriver.FindElements(By.XPath("//input[contains(@name,'tripType')]"));
            }
        }

        public IWebElement GetElemRoundTripRadio
        {
            get
            {
                return WebDriver.FindElement(By.XPath("//input[contains(@name,'tripType')][contains(@value,'roundtrip')]"));
            }
        }

        public IWebElement GetElemOneWayRadio
        {
            get
            {
                return WebDriver.FindElement(By.XPath("//input[contains(@name,'tripType')][contains(@value,'oneway')]"));
            }
        }

        public IWebElement GetElemPassengersDropdown
        {
            get
            {
                return WebDriver.FindElement(By.Name("passCount"));
            }
        }

        public IWebElement GetElemDepartingFromDropdown
        {
            get
            {
                return WebDriver.FindElement(By.Name("fromPort"));
            }
        }

        public IWebElement GetElemDepartingOnMonthDropdown
        {
            get
            {
                return WebDriver.FindElement(By.Name("fromMonth"));
            }
        }

        public IWebElement GetElemDepartingOnDayDropdown
        {
            get
            {
                return WebDriver.FindElement(By.Name("fromDay"));
            }
        }

        public IWebElement GetElemReturningFromDropdown
        {
            get
            {
                return WebDriver.FindElement(By.Name("toPort"));
            }
        }

        public IWebElement GetElemReturningOnMonthDropdown
        {
            get
            {
                return WebDriver.FindElement(By.Name("toMonth"));
            }
        }

        public IWebElement GetElemReturningOnDayDropdown
        {
            get
            {
                return WebDriver.FindElement(By.Name("toDay"));
            }
        }

        public ReadOnlyCollection<IWebElement> GetElemServiceClassRadio
        {
            get
            {
                return WebDriver.FindElements(By.XPath("//input[contains(@name,'servClass')]"));
            }
        }

        public IWebElement GetElemEconomyRadio
        {
            get
            {
                return WebDriver.FindElement(By.XPath("//input[contains(@name,'servClass')][contains(@value,'Coach')]"));
            }
        }

        public IWebElement GetElemBusinessRadio
        {
            get
            {
                return WebDriver.FindElement(By.XPath("//input[contains(@name,'servClass')][contains(@value,'Business')]"));
            }
        }

        public IWebElement GetElemFirstClassRadio
        {
            get
            {
                return WebDriver.FindElement(By.XPath("//input[contains(@name,'servClass')][contains(@value,'First')]"));
            }
        }

        public IWebElement GetElemAirlineDropdown
        {
            get
            {
                return WebDriver.FindElement(By.Name("airline"));
            }
        }

        public IWebElement GetElemContinueButton
        {
            get
            {
                return WebDriver.FindElement(By.Name("findFlights"));
            }
        }

        public string OpenFlightFinderURL()
        {
            string url = TDApp.GetAPPFlightFinderURL;
            WebDriver.OpenURL(url);
            return url;
        }

        public string SelectTripType()
        {
            return WebDriver.SelectRadio(GetElemTripTypeRadio);
        }

        public string SelectRoundTrip()
        {
            return WebDriver.SelectRadio(GetElemRoundTripRadio);
        }

        public string SelectOneWay()
        {
            return WebDriver.SelectRadio(GetElemOneWayRadio);
        }

        public string SelectPassengerCount(int value = 0)
        {
            return WebDriver.SelectOption(GetElemPassengersDropdown, value);
        }

        public string SelectDepartingFrom(int value = 0)
        {
            return WebDriver.SelectOption(GetElemDepartingFromDropdown, value);
        }

        public string SelectDepartingOnMonth(int value = 0)
        {
            return WebDriver.SelectOption(GetElemDepartingOnMonthDropdown, value);
        }

        public string SelectDepartingOnDay(int value = 0)
        {
            return WebDriver.SelectOption(GetElemDepartingOnDayDropdown, value);
        }

        public string SelectReturningFrom(int value = 0)
        {
            return WebDriver.SelectOption(GetElemReturningFromDropdown, value);
        }

        public string SelectReturningOnMonth(int value = 0)
        {
            return WebDriver.SelectOption(GetElemReturningOnMonthDropdown, value);
        }

        public string SelectReturningOnDay(int value = 0)
        {
            return WebDriver.SelectOption(GetElemReturningOnDayDropdown, value);
        }

        public string SelectServiceClass()
        {
            return WebDriver.SelectRadio(GetElemServiceClassRadio);
        }

        public string SelectEconomyClass()
        {
            return WebDriver.SelectRadio(GetElemEconomyRadio);
        }

        public string SelectBusinessClass()
        {
            return WebDriver.SelectRadio(GetElemBusinessRadio);
        }

        public string SelectFirstClassClass()
        {
            return WebDriver.SelectRadio(GetElemFirstClassRadio);
        }

        public string SelectAirline(int value = 0)
        {
            return WebDriver.SelectOption(GetElemAirlineDropdown, value);
        }

        public void ClickContinueButton()
        {
            WebDriver.ClickElement(GetElemContinueButton);
        }
    }
}
