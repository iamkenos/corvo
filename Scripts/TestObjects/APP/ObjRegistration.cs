﻿using Corvo.Common;
using Corvo.Scripts.TestData;
using OpenQA.Selenium;

namespace Corvo.Scripts.TestObjects.APP
{
    class ObjRegistration
    {
        public IWebElement GetElemFirstNameTextbox
        {
            get
            {
                return WebDriver.FindElement(By.Name("firstName"));
            }
        }

        public IWebElement GetElemLastNameTextbox
        {
            get
            {
                return WebDriver.FindElement(By.Name("lastName"));
            }
        }

        public IWebElement GetElemPhoneTextbox
        {
            get
            {
                return WebDriver.FindElement(By.Name("phone"));
            }
        }

        public IWebElement GetElemEmailTextbox
        {
            get
            {
                return WebDriver.FindElement(By.Name("userName"));
            }

        }

        public IWebElement GetElemAddress1Textbox
        {
            get
            {
                return WebDriver.FindElement(By.Name("address1"));
            }
        }

        public IWebElement GetElemAddress2Textbox
        {
            get
            {
                return WebDriver.FindElement(By.Name("address2"));
            }
        }

        public IWebElement GetElemCityTextbox
        {
            get
            {
                return WebDriver.FindElement(By.Name("city"));
            }
        }

        public IWebElement GetElemStateProvinceTextbox
        {
            get
            {
                return WebDriver.FindElement(By.Name("state"));
            }
        }

        public IWebElement GetElemPostalCodeTextbox
        {
            get
            {
                return WebDriver.FindElement(By.Name("postalCode"));
            }
        }

        public IWebElement GetElemCountryDropdown
        {
            get
            {
                return WebDriver.FindElement(By.Name("country"));
            }
        }

        public IWebElement GetElemUsernameTextbox
        {
            get
            {
                return WebDriver.FindElement(By.Name("email"));
            }
        }

        public IWebElement GetElemPasswordTextbox
        {
            get
            {
                return WebDriver.FindElement(By.Name("password"));
            }
        }

        public IWebElement GetElemConfirmPasswordTextbox
        {
            get
            {
                return WebDriver.FindElement(By.Name("confirmPassword"));
            }
        }

        public IWebElement GetElemSubmitButton
        {
            get
            {
                return WebDriver.FindElement(By.Name("register"));
            }
        }

        public IWebElement GetElemRegistrationConfirmation
        {
            get
            {
                return WebDriver.FindElement(By.XPath("//table[contains(.,'Thank you for registering')]"));
            }
        }

        public string OpenRegistrationURL()
        {
            string url = TDApp.GetAPPRegistrationURL;
            WebDriver.OpenURL(url);
            return url;
        }

        public string EnterContactInfoFirstName(string value = "")
        {
            return WebDriver.EnterTextAlphabetic(GetElemFirstNameTextbox, value);
        }

        public string EnterContactInfoLastName(string value = "")
        {
            return WebDriver.EnterTextAlphabetic(GetElemLastNameTextbox, value);
        }

        public string EnterContactInfoPhone(string value = "")
        {
            return WebDriver.EnterTextNumeric(GetElemPhoneTextbox, value, 8, 8);
        }

        public string EnterContactInfoEmail(string value = "")
        {
            return WebDriver.EnterTextEmail(GetElemEmailTextbox, value);
        }

        public string EnterMailingInfoAddress1(string value = "")
        {
            return WebDriver.EnterTextAlphanumeric(GetElemAddress1Textbox, value);
        }

        public string EnterMailingInfoAddress2(string value = "")
        {
            return WebDriver.EnterTextAlphanumeric(GetElemAddress2Textbox, value);
        }

        public string EnterMailingInfoCity(string value = "")
        {
            return WebDriver.EnterTextAlphabetic(GetElemCityTextbox, value);
        }

        public string EnterMailingInfoStateProvince(string value = "")
        {
            return WebDriver.EnterTextAlphabetic(GetElemStateProvinceTextbox, value);
        }

        public string EnterMailingInfoPostalCode(string value = "")
        {
            return WebDriver.EnterTextNumeric(GetElemPostalCodeTextbox, value, 6, 6);
        }

        public string EnterMailingInfoCountry(int value = 0)
        {
            return WebDriver.SelectOption(GetElemCountryDropdown, value);
        }

        public string EnterUserInfoUsername(string value = "")
        {
            return WebDriver.EnterTextAlphanumeric(GetElemUsernameTextbox, value);
        }

        public string EnterUserInfoPassword(string value = "")
        {
            return WebDriver.EnterTextAlphanumeric(GetElemPasswordTextbox, value);
        }

        public string EnterUserInfoConfirmPassword(string value = "")
        {
            return WebDriver.EnterTextAlphanumeric(GetElemConfirmPasswordTextbox, value);
        }

        public void ClickSubmitButton()
        {
            WebDriver.ClickElement(GetElemSubmitButton);
        }
    }
}
