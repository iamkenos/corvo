﻿using Corvo.Common;
using Corvo.Scripts.TestData;
using OpenQA.Selenium;

namespace Corvo.Scripts.TestObjects.APP
{
    class ObjSignOn
    {
        public IWebElement GetElemUsernameTextbox
        {
            get
            {
                return WebDriver.FindElement(By.Name("userName"));
            }
        }

        public IWebElement GetElemPasswordTextbox
        {
            get
            {
                return WebDriver.FindElement(By.Name("password"));
            }
        }

        public IWebElement GetElemSubmitButton
        {
            get
            {
                return WebDriver.FindElement(By.Name("login"));
            }
        }

        public IWebElement GetElemSignOff
        {
            get
            {
                return WebDriver.FindElement(By.XPath("//a[contains(text(),'SIGN-OFF')]"));
            }
        }

        public string OpenSignOnURL()
        {
            string url = TDApp.GetAPPSignOnURL;
            WebDriver.OpenURL(url);
            return url;
        }

        public string EnterUsername(string value = "")
        {
            return WebDriver.EnterTextAlphanumeric(GetElemUsernameTextbox, value);
        }

        public string EnterPassword(string value = "")
        {
            return WebDriver.EnterTextAlphanumeric(GetElemPasswordTextbox, value);
        }

        public void ClickSubmitButton()
        {
            WebDriver.ClickElement(GetElemSubmitButton);
        }
    }
}
