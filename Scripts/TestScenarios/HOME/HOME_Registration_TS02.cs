﻿using Corvo.Common;
using Corvo.Scripts.TestObjects.APP;
using Corvo.Scripts.TestProcedures.APP;
using OpenQA.Selenium;

namespace Corvo.Scripts.TestScenarios.HOME
{
    class HOME_Registration_TS02
    {
        ObjRegistration objRegistration = new ObjRegistration();
        ProcRegistration procRegistration = new ProcRegistration();

        public void GivenFillsUpRegistrationForm()
        {
            GlobalRunner.Run(procRegistration);
        }

        public void ThenUserClicksSubmit()
        {
            IWebElement elem = objRegistration.GetElemRegistrationConfirmation;

            bool result = WebDriver.VerifyElementPresent(elem);
            GlobalReporter.WriteResult(result, "TC01");

            WebDriver.WaitUntilElemIsDisplayed(objRegistration.GetElemAddress1Textbox);
        }
    }
}
