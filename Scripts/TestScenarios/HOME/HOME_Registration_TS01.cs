﻿using Corvo.Scripts.TestObjects.APP;
using Corvo.Common;
using OpenQA.Selenium;

namespace Corvo.Scripts.TestScenarios.HOME
{
    class HOME_Registration_TS01
    {
        ObjRegistration objRegistration = new ObjRegistration();

        string firstName;
        string lastName;
        string phoneNum;
        string email;
        string address1;
        string address2;
        string city;
        string state;
        string postal;
        string country;
        string username;
        string password;
        string confirmPassword;

        public void GivenRegistrationPageIsOpened()
        {
            bool result = WebDriver.VerifyPageURL(objRegistration.OpenRegistrationURL());
            GlobalReporter.WriteResult(result, "TC01");
        }

        public void WhenUserFillsUpContactInfo()
        {
            firstName = objRegistration.EnterContactInfoFirstName();
            lastName = objRegistration.EnterContactInfoLastName();
            phoneNum = objRegistration.EnterContactInfoPhone();
            email = objRegistration.EnterContactInfoEmail();
            bool result = firstName.Equals(WebDriver.GetElementValue(objRegistration.GetElemFirstNameTextbox))
                && lastName.Equals(WebDriver.GetElementValue(objRegistration.GetElemLastNameTextbox))
                && phoneNum.Equals(WebDriver.GetElementValue(objRegistration.GetElemPhoneTextbox))
                && email.Equals(WebDriver.GetElementValue(objRegistration.GetElemEmailTextbox));

            GlobalReporter.WriteResult(result, "TC02");
        }

        public void WhenUserFillsUpMailingInfo()
        {
            address1 = objRegistration.EnterMailingInfoAddress1();
            address2 = objRegistration.EnterMailingInfoAddress2();
            city = objRegistration.EnterMailingInfoCity();
            state = objRegistration.EnterMailingInfoStateProvince();
            postal = objRegistration.EnterMailingInfoPostalCode();
            country = objRegistration.EnterMailingInfoCountry();

            bool result = address1.Equals(WebDriver.GetElementValue(objRegistration.GetElemAddress1Textbox))
                && address2.Equals(WebDriver.GetElementValue(objRegistration.GetElemAddress2Textbox))
                && city.Equals(WebDriver.GetElementValue(objRegistration.GetElemCityTextbox))
                && state.Equals(WebDriver.GetElementValue(objRegistration.GetElemStateProvinceTextbox))
                && postal.Equals(WebDriver.GetElementValue(objRegistration.GetElemPostalCodeTextbox))
                && country.Equals(WebDriver.GetSelectedDropdownText(objRegistration.GetElemCountryDropdown));

            GlobalReporter.WriteResult(result, "TC03");
        }

        public void WhenUserFillsUpUserInfo()
        {
            username = objRegistration.EnterUserInfoUsername();
            password = objRegistration.EnterUserInfoPassword();
            confirmPassword = objRegistration.EnterUserInfoConfirmPassword(password);

            bool result = username.Equals(WebDriver.GetElementValue(objRegistration.GetElemUsernameTextbox))
                && password.Equals(WebDriver.GetElementValue(objRegistration.GetElemPasswordTextbox))
                && confirmPassword.Equals(WebDriver.GetElementValue(objRegistration.GetElemConfirmPasswordTextbox));

            GlobalReporter.WriteResult(result, "TC04");
        }

        public void ThenUserClicksSubmit()
        {
            objRegistration.ClickSubmitButton();

            IWebElement elem = objRegistration.GetElemRegistrationConfirmation;

            bool result = WebDriver.VerifyElementPresent(elem)
            && WebDriver.VerifyElementContainsText(elem, firstName + " " + lastName)
            && WebDriver.VerifyElementContainsText(elem, "Note: Your user name is " + username);

            GlobalReporter.WriteResult(result, "TC05");
        }
    }
}
