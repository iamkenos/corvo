﻿using Corvo.Common;
using Corvo.Scripts.TestData;
using Corvo.Scripts.TestObjects.APP;
using Corvo.Scripts.TestProcedures.APP;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;

namespace Corvo.Scripts.TestScenarios.HOME
{
    class HOME_SignOn_TS01
    {
        ProcSignOn procSignOn;
        ProcRegistration procRegistration;
        ObjSignOn objSignOn = new ObjSignOn();

        Dictionary<string, bool> verificationsTC01 = new Dictionary<string, bool>();

        public void GivenFillsUpRegistrationForm()
        {
            procRegistration = new ProcRegistration();
            GlobalRunner.Run(procRegistration);

            bool result = WebDriver.VerifyElementPresent(By.XPath("//table//p[contains(.,'Your user name is')]"));
            verificationsTC01.Add(WebDriver.TakeScreenshot(), result);
        }

        public void WhenUserSignOn()
        {
            procSignOn = new ProcSignOn(TDApp.GetAPPUsername, TDApp.GetAPPPassword);
            GlobalRunner.Run(procSignOn);
        }

        public void ThenUserIsSuccessfullySignedOn()
        {
            bool result = WebDriver.VerifyElementPresent(objSignOn.GetElemSignOff);
            verificationsTC01.Add(WebDriver.TakeScreenshot(), result);
            GlobalReporter.WriteAllStepResults(verificationsTC01, "TC01", string.Format("User is successfully logged in.<br>Username: {0}<br>Password: {1}", procSignOn.username, procSignOn.password));
        }
    }
}
