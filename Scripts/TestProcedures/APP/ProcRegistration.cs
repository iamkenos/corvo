﻿using Corvo.Scripts.TestObjects.APP;
using Corvo.Common;

namespace Corvo.Scripts.TestProcedures.APP
{
    class ProcRegistration
    {
        ObjRegistration objRegistration = new ObjRegistration();

        public void OpenRegistrationURL()
        {
            WebDriver.VerifyPageURL(objRegistration.OpenRegistrationURL());
        }

        public void FillUpContactInformation()
        {
            objRegistration.EnterContactInfoFirstName();
            objRegistration.EnterContactInfoLastName();
            objRegistration.EnterContactInfoPhone();
            objRegistration.EnterContactInfoEmail();
        }

        public void FillUpMailingInformation()
        {
            objRegistration.EnterMailingInfoAddress1();
            objRegistration.EnterMailingInfoAddress2();
            objRegistration.EnterMailingInfoCity();
            objRegistration.EnterMailingInfoStateProvince();
            objRegistration.EnterMailingInfoPostalCode();
            objRegistration.EnterMailingInfoCountry();
        }

        public void FillUpUserInformation()
        {
            string username;
            string password;

            username = objRegistration.EnterUserInfoUsername();
            password = objRegistration.EnterUserInfoPassword();
            objRegistration.EnterUserInfoConfirmPassword(password);

            ExcelAdapter.UpdateData("APP", "APPUsername", username);
            ExcelAdapter.UpdateData("APP", "APPPassword", password);
        }

        public void ClickSubmitButton()
        {
            objRegistration.ClickSubmitButton();
        }
    }
}
