﻿using Corvo.Common;
using Corvo.Scripts.TestObjects.APP;

namespace Corvo.Scripts.TestProcedures.APP
{
    class ProcSignOn
    {
        ObjSignOn objSignOn = new ObjSignOn();
        public string username = "";
        public string password = "";

        public ProcSignOn(string username = "", string password = "")
        {
            this.username = username;
            this.password = password;
        }

        public void OpenSignOnURL()
        {
            WebDriver.VerifyPageURL(objSignOn.OpenSignOnURL());
        }

        public void FillUpSignOnInformation()
        {
            objSignOn.EnterUsername(username);
            objSignOn.EnterPassword(password);
        }

        public void ClickSubmitButton()
        {
            objSignOn.ClickSubmitButton();
        }
    }
}
