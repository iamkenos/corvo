﻿using Corvo.Scripts.TestObjects.FLIGHTS;
using Corvo.Common;

namespace Corvo.Scripts.TestProcedures.FLIGHTS
{
    class ProcFlightFinder
    {
        ObjFlightFinder objFlightFinder = new ObjFlightFinder();

        public void OpenRegistrationURL()
        {
            WebDriver.VerifyPageURL(objFlightFinder.OpenFlightFinderURL());
        }

        public void FillUpFlightDetails()
        {
            objFlightFinder.SelectTripType();
            objFlightFinder.SelectPassengerCount();
            objFlightFinder.SelectDepartingFrom();
            objFlightFinder.SelectDepartingOnMonth();
            objFlightFinder.SelectDepartingOnDay();
            objFlightFinder.SelectReturningFrom();
            objFlightFinder.SelectReturningOnMonth();
            objFlightFinder.SelectReturningOnDay();
        }

        public void FillUpPreferences()
        {
            objFlightFinder.SelectServiceClass();
            objFlightFinder.SelectAirline();
        }

        public void ClickSubmitButton()
        {
            objFlightFinder.ClickContinueButton();
        }
    }
}
