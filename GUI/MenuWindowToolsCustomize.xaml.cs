﻿using Corvo.Common;
using Corvo.GUI.CustomControls;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Corvo.GUI
{
    public partial class MenuWindowToolsCustomize : Window
    {
        GlobalLogger Logger = new GlobalLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public MenuWindowToolsCustomize(int x, int y)
        {
            InitializeComponent();
            this.Left = x - this.Width / 2;
            this.Top = y - this.Height / 2;

            RdBtnTestExecMode_LoadValues(RdBtnTextExec1, new RoutedEventArgs(), GlobalUtils.testExecModeValue1);
            RdBtnTestExecMode_LoadValues(RdBtnTextExec2, new RoutedEventArgs(), GlobalUtils.testExecModeValue2);
            Window_Load();
        }

        //=======================================================================WINDOW CONTROLS========================================================================
        private void Window_Load()
        {
            try
            {
                //---TEST EXECUTION TYPE
                GetTestExecMode();
                TbxConcurrentTests.Text = GlobalUtils.concurrentTests;
                TbxConcurrentTests.IsEnabled = Convert.ToBoolean(RdBtnTextExec2.IsChecked);
                //---GLOBAL TIMEOUT
                TbxGlobalTimeout.Text = GlobalUtils.globalTimeout;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key.Equals(Key.Escape))
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //=======================================================================WINDOW CONTROLS========================================================================

        //========================================================================CONTROL BUTTONS=======================================================================
        private void BtnMenuWindowToolsCustomizeSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SetTestExecMode();
                ConfigAdapter.SetValue(GlobalUtils.concurrentTestsKey, TbxConcurrentTests.Text);
                ConfigAdapter.SetValue(GlobalUtils.globalTimeoutKey, TbxGlobalTimeout.Text);
                MainWindow.MainAppWindow.GetTestExecMode();
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void BtnMenuWindowToolsCustomizeCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //========================================================================CONTROL BUTTONS=======================================================================

        //=============================================================================LABELS===========================================================================
        private void LblGlobalTimeout_Click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                TbxGlobalTimeout.Focus();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void LblTestExecMode_Click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                TbxConcurrentTests.Focus();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //=============================================================================LABELS===========================================================================

        //==========================================================================TEXT BOXES==========================================================================
        private void TbxGlobalTimeout_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                TbxGlobalTimeout_VerifyValue(this, e);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void TbxGlobalTimeout_VerifyValue(object sender, RoutedEventArgs e)
        {
            try
            {
                TextboxMasking.TbxVerifyValue(TbxGlobalTimeout, LblGlobalTimeoutError, BtnMenuWindowToolsCustomizeSave, @"^([5-9]|([1-9]\d)|([1][0-1]\d)|([1][0-2][0]))$");
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //==========================================================================TEXT BOXES==========================================================================

        //=========================================================================RADIO BUTTONS========================================================================
        private void RdBtnTextExec_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RadioButton rdBtntestExec = sender as RadioButton;
                if (rdBtntestExec.Content.Equals(GlobalUtils.testExecModeValue2))
                {
                    TbxConcurrentTests.IsEnabled = true;
                }
                else
                {
                    TbxConcurrentTests.IsEnabled = false;
                }  
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void RdBtnTestExecMode_LoadValues(object sender, RoutedEventArgs e, string value)
        {
            try
            {
                RadioButton rdBtntestExec = sender as RadioButton;
                rdBtntestExec.Content = value;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void GetTestExecMode()
        {
            switch (GlobalUtils.testExecMode)
            {
                case GlobalUtils.testExecModeValue1:
                    RdBtnTextExec1.IsChecked = true;
                    break;
                case GlobalUtils.testExecModeValue2:
                    RdBtnTextExec2.IsChecked = true;
                    break;
                default:
                    goto case GlobalUtils.testExecModeValue1;
            }
        }

        private void SetTestExecMode()
        {
            if (RdBtnTextExec1.IsChecked.Equals(true))
            {
                ConfigAdapter.SetValue(GlobalUtils.testExecModeKey, GlobalUtils.testExecModeValue1);
            }
            else
            {
                ConfigAdapter.SetValue(GlobalUtils.testExecModeKey, GlobalUtils.testExecModeValue2);
            }
        }
        //=========================================================================RADIO BUTTONS========================================================================
    }
}
