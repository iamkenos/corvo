﻿namespace Corvo.GUI.Objects
{
    public class ScriptProperties
    {
        public string Number { get; set; }
        public string TestName { get; set; }
        public string Status { get; set; }
        public double Percentage { get; set; }
    }
}
