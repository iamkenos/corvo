﻿using Corvo.Common;
using System.Reflection;
using System.Windows;

namespace Corvo.GUI
{
    public partial class App : Application
    {
        GlobalLogger Logger = new GlobalLogger(MethodBase.GetCurrentMethod().DeclaringType);

        void StartApp(object sender, StartupEventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
        }
    }
}
