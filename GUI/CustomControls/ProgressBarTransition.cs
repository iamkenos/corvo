﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Corvo.GUI.CustomControls
{
    public static class ProgressBarTransition
    {
        private static TimeSpan duration = TimeSpan.FromSeconds(.5);

        public static void SetPercent(ProgressBar progressBar, double percentage)
        {
            DoubleAnimation animation = new DoubleAnimation(percentage, duration);
            progressBar.BeginAnimation(ProgressBar.ValueProperty, animation);
        }

        //=========================================================================CONTROL UTILS========================================================================
        /// <summary>
        /// Used for accessing control items nested under a [DataGridTemplateColumn].
        /// </summary>
        /// <param name="parent">x:Name property of the DataGrid control item</param>
        /// <param name="childName">x:Name of the control item you want to access nested under the [DataGridTemplateColumn]</param>
        /// <param name="list">Provide a list of type [ControlItem] to be populate</param>
        /// Usage:
        /// 1. Declare a list of type [ControlItem]
        ///    >>> List<ProgressBar> progressbarList = new List<ProgressBar>();
        /// 2. Call the function to populate the list
        ///    >>> ProgressBarTransition.FindChildGroup(MainWindow.MainAppWindow.DataGridScriptsViewer, "PrgBarTestPercentage", ref progressbarList);
        /// 3. Use the list to interact with the [ControlItem]
        public static void FindChildGroup<T>(DependencyObject parent, string childName, ref List<T> list) where T : DependencyObject
        {
            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);

            for (int i = 0; i < childrenCount; i++)
            {
                //Get the child
                var child = VisualTreeHelper.GetChild(parent, i);

                //Compare on conformity the type
                T child_Test = child as T;

                //Don't compare - skip
                if (child_Test == null)
                {
                    //Go deeper
                    FindChildGroup<T>(child, childName, ref list);
                }
                else
                {
                    //If match, then check the name of the item
                    FrameworkElement child_Element = child_Test as FrameworkElement;

                    if (child_Element.Name == childName)
                    {
                        //Found
                        list.Add(child_Test);
                    }

                    //Recursive checking
                    FindChildGroup<T>(child, childName, ref list);
                }
            }
            return;
        }
        //=============================================================================ABOUT============================================================================
    }
}
