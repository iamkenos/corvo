﻿using Corvo.Common;
using Corvo.GUI.CustomControls;
using Corvo.GUI.Objects;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace Corvo.GUI
{
    /* ****************************************************************************************************************
     * Author: Alex Matunog
     * Project Name: Corvo
     * Date Created: 14/07/2016
     * Modification Logs **********************************************************************************************
     * 1. Version 1.0 - 03/10/2016
     *  - Version 1.3 of Altair.exe
     * 2. Version 1.1 - 05/10/2016
     *  - Modified UI
     * 3. Version 1.2 - 15/10/2016
     *  - Major changes on test scripts main panel
     *  - Added progress bar panel in prep for concurrent running
     *  - Added customize menu option to serve as a toggle for single and multi test execution
     *  - Removed strong and medium matches of cloned code
     *  - Bug fixes on report generation for custom test scenario names
     * 4. Version 1.3 - 30/10/2016
     *  - Added geckodriver to support firefox version 47 and above
     *  - Fixed customize menu - GlobalTimeout textbox masking
     * ****************************************************************************************************************
     * TO DO: Version 2.0
     *  - Add EdgeDriver support
     *  - Multi-threading
     *  - Import settings
     * **************************************************************************************************************** */

    public partial class MainWindow : Window
    {
        GlobalLogger Logger = new GlobalLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static MainWindow MainAppWindow;

        public GlobalScheduler Scheduler;
        public Type[] FilteredFiles;
        public Type[] FilteredFilesCurrent;

        public MainWindow()
        {

            InitializeComponent();
            Window_Load();

            Logger.Info("=================================== Running " + GlobalUtils.version + " ===================================");
        }
        //=======================================================================WINDOW CONTROLS========================================================================
        private void Window_Load()
        {
            try
            {
                var desktopWorkingArea = SystemParameters.WorkArea;
                this.Left = desktopWorkingArea.Right - this.Width;
                this.Top = desktopWorkingArea.Bottom - this.Height;
                this.Topmost = true;

                MainAppWindow = this;
                GetTestExecMode();
                ApplyScriptsFilter();

                Window_UpdateSettingsPanel();
                Window_UpdateStatus(string.Format("{0} {1}", GlobalUtils.lblStatus, GlobalUtils.lblStatusReady));

                ConfigAdapter.SetValue(GlobalUtils.versionKey, string.Format("{0} v{1}", GlobalUtils.assemblyTitle, Assembly.GetExecutingAssembly().GetName().Version));
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void Window_MoveMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (e.LeftButton.Equals(MouseButtonState.Pressed))
                {
                    DragMove();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void WindowControls_Minimize(object sender, RoutedEventArgs e)
        {
            try
            {
                this.WindowState = WindowState.Minimized;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void WindowControls_Exit(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
                EndProcess(GlobalUtils.assemblyTitle);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void Window_UpdateStatus(string status)
        {
            LblStatus.Content = status;
        }

        public void Window_UpdateSettingsPanel()
        {
            LblSettingsMod.Content = string.Format("{0}: {1}", GlobalUtils.lblModule, GlobalUtils.testModule);
            LblSettingsEnv.Content = string.Format("{0}: {1}", GlobalUtils.lblEnvironment, GlobalUtils.testEnvironment);
            LblSettingsBrowser.Content = string.Format("{0}: {1}", GlobalUtils.lblBrowser, GlobalUtils.webBrowserName);

            if (Convert.ToBoolean(GlobalUtils.scheduleRunEnabled))
            {
                LblSettingsSched.Content = string.Format("{0}: {1}", GlobalUtils.lblScheduler, GlobalUtils.scheduleRun);
            }
            else
            {
                LblSettingsSched.Content = string.Format("{0}: {1}", GlobalUtils.lblScheduler, GlobalUtils.lblDisabled);
            }

            if (Convert.ToBoolean(GlobalUtils.loopsEnabled))
            {
                LblSettingsLoops.Content = string.Format("{0}: {1} - {2}", GlobalUtils.lblLoops, GlobalUtils.loopsType, GlobalUtils.loops);
            }
            else
            {
                LblSettingsLoops.Content = string.Format("{0}: {1}", GlobalUtils.lblLoops, GlobalUtils.lblDisabled);
            }
        }
        //=======================================================================WINDOW CONTROLS========================================================================

        //=====================================================================MAIN CONTROL BUTTONS=====================================================================
        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BtnStart.Content.ToString().Equals(GlobalUtils.btnLblStart))
                {
                    StartProcess();
                }
                else
                {
                    GuiTeardown(GlobalUtils.lblStatusStopped);
                    GlobalUtils.InterruptFlag.Reset();
                    GlobalUtils.RunFlag = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void BtnPause_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BtnPause.Content.ToString().Equals(GlobalUtils.btnLblPause))
                {
                    GlobalUtils.InterruptFlag.Reset();
                    Window_UpdateStatus(string.Format("{0} {1}", GlobalUtils.lblStatus, GlobalUtils.lblStatusPaused));
                    BtnPause.Content = GlobalUtils.btnLblResume;
                }
                else
                {
                    GlobalUtils.InterruptFlag.Set();
                    Window_UpdateStatus(string.Format("{0} {1}", GlobalUtils.lblStatus, GlobalUtils.lblStatusRunning));
                    BtnPause.Content = GlobalUtils.btnLblPause;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void BtnReport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GlobalReporter.GenerateReport();
                GlobalReporter.GenerateOverallReport();
                Process.Start(GlobalUtils.resultsHtmlPath);
            }
            catch (Exception ex)
            {
                MenuFileResultsOpenResults_Click(this, e);
                LblTestMethod_SetText(GlobalUtils.lblTestMethodReportError);
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public void StartProcess()
        {
            try
            {
                #region Setup: Do Pre-Conditions
                GuiSetup();
                DirsSetup();
                GlobalUtils.RunFlag = true;
                #endregion

                GlobalUtils.ProcessThread = new Thread(() => ExecuteTests(FilteredFilesCurrent));
                GlobalUtils.ProcessThread.Start();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void EndProcess(string processName)
        {
            try
            {
                foreach (Process process in Process.GetProcessesByName(processName))
                {
                    process.Kill();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void ExecuteTests(Type[] filteredFiles)
        {
            try
            {
                #region PROCESS: Run tests based on loops
                int loops = CheckLoops();
                for (int foo = 0; foo < loops; foo++)
                {
                    if (GlobalUtils.RunFlag)
                    {
                        int currentIter = foo + 1;
                        ConfigAdapter.SetValue(GlobalUtils.currentIterKey, currentIter.ToString());
                        GlobalUtils.InterruptFlag = new ManualResetEvent(true);

                        #region GUI: Left Panel Status Update - Running
                        GlobalUtils.InvokeUI(() => Window_UpdateStatus(string.Format("{0} {1}", GlobalUtils.lblStatus, GlobalUtils.lblStatusRunning)));
                        #endregion

                        #region Interrupt Flag: Interrupt on click of Pause / Stop buttons
                        GlobalUtils.InterruptFlag.WaitOne();
                        #endregion

                        #region App: Run Test Scenarios
                        for (int bar = 0; bar < filteredFiles.Length; bar++)
                        {
                            if (GlobalUtils.RunFlag)
                            {
                                try
                                {
                                    DriverSetup();
                                    #region GUI: Update Main Panel
                                    string testScenario = filteredFiles[bar].Name;
                                    if (GlobalUtils.testExecMode.Equals(GlobalUtils.testExecModeValue1))
                                    {
                                        GlobalUtils.InvokeUI(() => TblockScriptsViewer_UpdateProgress(GlobalUtils.progressUpdate1, GlobalUtils.brushNewTest, testScenario));
                                    }
                                    #endregion
                                    GlobalRunner.Run(filteredFiles[bar], bar);
                                    #region GUI: Refresh LblTestMethod content
                                    GlobalUtils.InvokeUI(() => LblTestMethod_SetText(""));
                                    #endregion
                                    DriverTeardown();
                                }
                                catch (Exception ex)
                                {
                                    #region GUI: Exit test and update main panel on exception
                                    string testScenario = filteredFiles[bar].Name;
                                    string testDescription = testScenario;

                                    GlobalUtils.InvokeUI(() => testDescription = LblTestMethod.Content.ToString());
                                    Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);

                                    GlobalReporter.WriteResult(false, GlobalUtils.tableLabelTCBlocked, "An exception was thrown while running the test.<br/>" + testDescription, testScenario);
                                    DriverTeardown();
                                    #endregion
                                }
                            }
                            else
                            {
                                return;
                            }
                        }
                        #endregion

                        ConfigAdapter.SetValue(GlobalUtils.testTimeStampEndKey, DateTime.Now.ToString());

                        #region GUI: Main Panel Update - Completed
                        if (loops > 1)
                        {
                            if (GlobalUtils.testExecMode.Equals(GlobalUtils.testExecModeValue1))
                            {
                                GlobalUtils.InvokeUI(() => TblockScriptsViewer_UpdateProgress(GlobalUtils.progressUpdate4, GlobalUtils.brushLoops, null, foo));
                            }
                        }
                        else
                        {
                            if (GlobalUtils.testExecMode.Equals(GlobalUtils.testExecModeValue1))
                            {
                                GlobalUtils.InvokeUI(() => TblockScriptsViewer_UpdateProgress(GlobalUtils.progressUpdate3, GlobalUtils.brushLoops));
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        return;
                    }
                }
                #endregion

                #region GUI: Left Panel Status Update & Controls Cleanup
                GlobalUtils.InvokeUI(() => GuiTeardown(GlobalUtils.lblStatusCompleted));
                #endregion
            }
            catch (Exception ex)
            {
                #region GUI: Left Panel Status Update & Controls Cleanup
                GlobalUtils.InvokeUI(() => GuiTeardown(GlobalUtils.lblStatusReady));
                #endregion
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private int CheckLoops()
        {
            int loops = 1;
            if (Convert.ToBoolean(GlobalUtils.loopsEnabled))
            {
                loops = int.Parse(GlobalUtils.loops);
            }

            if (loops > 1)
            {
                ConfigAdapter.SetValue(GlobalUtils.loopedRunKey, "true");
            }
            else
            {
                ConfigAdapter.SetValue(GlobalUtils.loopedRunKey, "false");
            }

            return loops;
        }
        //=====================================================================MAIN CONTROL BUTTONS=====================================================================

        //========================================================================MAIN MENU ITEMS=======================================================================
        private void MenuFileLogs_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProcessStartInfo runExplorer = new ProcessStartInfo();
                runExplorer.FileName = "explorer.exe";
                runExplorer.Arguments = GlobalUtils.logsPath;
                Process.Start(runExplorer);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void MenuFileResultsSaveResults_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GlobalReporter.GenerateOverallReport();
                GlobalReporter.ZipResults(GlobalUtils.resultsOverallPath);
            }
            catch (Exception ex)
            {
                MenuFileResultsOpenResults_Click(this, e);
                LblTestMethod_SetText(GlobalUtils.lblTestMethodReportError);
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void MenuFileResultsOpenResults_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(GlobalUtils.resultsRootPath));
                ProcessStartInfo runExplorer = new ProcessStartInfo();
                runExplorer.FileName = "explorer.exe";
                runExplorer.Arguments = GlobalUtils.resultsRootPath;
                Process.Start(runExplorer);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void MenuFileExit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                WindowControls_Exit(this, e);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void MenuToolsSettings_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int x = int.Parse(Application.Current.MainWindow.Left.ToString()) + (int)this.Width / 2;
                int y = int.Parse(Application.Current.MainWindow.Top.ToString()) + (int)this.Height / 2;
                MenuWindowToolsSettings menuWindowToolsSettings = new MenuWindowToolsSettings(x, y);
                menuWindowToolsSettings.Topmost = true;
                menuWindowToolsSettings.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void MenuToolsCustomize_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int x = int.Parse(Application.Current.MainWindow.Left.ToString()) + (int)this.Width / 2;
                int y = int.Parse(Application.Current.MainWindow.Top.ToString()) + (int)this.Height / 2;
                MenuWindowToolsCustomize menuWindowToolsCustomize = new MenuWindowToolsCustomize(x, y);
                menuWindowToolsCustomize.Topmost = true;
                menuWindowToolsCustomize.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void MenuToolsTestData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProcessStartInfo runExplorer = new ProcessStartInfo();
                runExplorer.FileName = "explorer.exe";
                runExplorer.Arguments = GlobalUtils.testDataPath;
                Process.Start(runExplorer);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void MenuToolsCleanUp_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int x = int.Parse(Application.Current.MainWindow.Left.ToString()) + (int)this.Width / 2;
                int y = int.Parse(Application.Current.MainWindow.Top.ToString()) + (int)this.Height / 2;
                MenuWindowWarning menuWindowWarning = new MenuWindowWarning(x, y);
                menuWindowWarning.Topmost = true;
                menuWindowWarning.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void MenuHelpAbout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int x = int.Parse(Application.Current.MainWindow.Left.ToString()) + (int)this.Width / 2;
                int y = int.Parse(Application.Current.MainWindow.Top.ToString()) + (int)this.Height / 2;
                MenuWindowHelpAbout menuWindowHelpAbout = new MenuWindowHelpAbout(x, y);
                menuWindowHelpAbout.Topmost = true;
                menuWindowHelpAbout.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void MenuHelpViewHelp_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(GlobalUtils.viewHelpPath);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //========================================================================MAIN MENU ITEMS=======================================================================

        //======================================================================TEST SCRIPTS FILTER=====================================================================
        private void TbxScriptsFilter_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key.Equals(Key.Return))
                {
                    Window_UpdateStatus(string.Format("{0} {1}", GlobalUtils.lblStatus, GlobalUtils.lblStatusReady));
                    ConfigAdapter.SetValue(GlobalUtils.scriptsFilterKey, TbxScriptsFilter.Text);
                    DisplayScripts(FilteredFiles);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void DataGridScriptsViewer_ResetProgress()
        {
            for (int foo = 0; foo < DataGridScriptsViewer.Items.Count; foo++)
            {
                ScriptProperties scripts = (ScriptProperties)DataGridScriptsViewer.Items[foo];
                DataGridScriptsViewer.Items[foo] = (new ScriptProperties
                {
                    Number = scripts.Number,
                    Percentage = 0,
                    TestName = scripts.TestName
                });
            }
        }

        private void TblockScriptsViewerNum_InsertBreakLine(string brushColor)
        {
            BrushConverter brushConverter = new BrushConverter();
            TblockScriptsViewerNum.Inlines.Add(new Run(string.Format("{0}{1}", GlobalUtils.lineBreak, Environment.NewLine)) { Foreground = (Brush)brushConverter.ConvertFromString(brushColor) });
        }

        private void TblockScriptsViewer_InsertBreakLine(string brushColor)
        {
            BrushConverter brushConverter = new BrushConverter();
            TblockScriptsViewer.Inlines.Add(new Run(string.Format("{0}{1}", GlobalUtils.lineBreak, Environment.NewLine)) { Foreground = (Brush)brushConverter.ConvertFromString(brushColor) });
        }

        private void TblockScriptsViewer_UpdateProgress(string progressUpdate, string brushColor, string testScenario = null, int foo = 0)
        {
            try
            {
                BrushConverter brushConverter = new BrushConverter();
                TblockScriptsViewer_InsertBreakLine(brushColor);
                switch (progressUpdate)
                {
                    case GlobalUtils.progressUpdate1:
                        ConfigAdapter.SetValue(GlobalUtils.testTimeStampStartKey, DateTime.Now.ToString());
                        string testTimeStampStart = GlobalUtils.testTimeStampStart.Substring(GlobalUtils.testTimeStampStart.IndexOf(' ') + 1);
                        TblockScriptsViewer.Inlines.Add(new Run(string.Format("[{0}] {1}{2}", testTimeStampStart, testScenario, Environment.NewLine)) { Foreground = (Brush)brushConverter.ConvertFromString(brushColor) });
                        break;
                    case GlobalUtils.progressUpdate2:
                        TblockScriptsViewer.Inlines.Add(new Run(string.Format("STOPPED {0}{1}", DateTime.Now.ToString(), Environment.NewLine)) { Foreground = (Brush)brushConverter.ConvertFromString(brushColor) });
                        break;
                    case GlobalUtils.progressUpdate3:
                        TblockScriptsViewer.Inlines.Add(new Run(string.Format("COMPLETED {0}{1}", GlobalUtils.testTimeStampEnd, Environment.NewLine)) { Foreground = (Brush)brushConverter.ConvertFromString(brushColor) });
                        break;
                    case GlobalUtils.progressUpdate4:
                        TblockScriptsViewer.Inlines.Add(new Run(string.Format("COMPLETED ITERATION {0} {1}{2}", (foo + 1), GlobalUtils.testTimeStampEnd, Environment.NewLine)) { Foreground = (Brush)brushConverter.ConvertFromString(brushColor) });
                        break;
                }
                TblockScriptsViewer_InsertBreakLine(brushColor);
                TblockScriptsViewerNum_InsertBreakLine(brushColor);
                TblockScriptsViewerNum.Inlines.Add(Environment.NewLine);
                TblockScriptsViewerNum_InsertBreakLine(brushColor);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public void DataGridScriptsViewer_SetProgress(int scriptIndex)
        {
            DataGridScriptsViewer.Items[scriptIndex] = (new ScriptProperties
            {
                Number = GlobalRunner.number,
                TestName = GlobalRunner.testName
            });
            List<ProgressBar> progressbarList = GetPrgBarTestPercentage();
            ProgressBar[] progressbarArray = progressbarList.ToArray();
            ProgressBarTransition.SetPercent(progressbarArray[scriptIndex], GlobalRunner.percentage);
        }

        public void TblockScriptsViewer_SetText(string foo)
        {
            TblockScriptsViewerNum.Text = foo;
            TblockScriptsViewer.Text = foo;
        }

        public void LblTestMethod_SetText(string foo)
        {
            LblTestMethod.Content = foo;
        }

        public void LblTestMethod_ShowRunningClass(string className, string methodName)
        {
            string runningProcedure = (string)LblTestMethod.Content;
            if (runningProcedure.Contains("|"))
            {
                runningProcedure = runningProcedure.Remove(runningProcedure.LastIndexOf(" | ")) + " | ";
            }
            else
            {
                runningProcedure = runningProcedure + " | ";
            }

            LblTestMethod_SetText(runningProcedure + className + ": " + methodName);
        }

        public void GetTestExecMode()
        {
            switch (GlobalUtils.testExecMode)
            {
                case GlobalUtils.testExecModeValue1:
                    DockPanelScriptsViewer.Visibility = Visibility.Visible;
                    DataGridScriptsViewer.Visibility = Visibility.Hidden;
                    ScrollViewerTestScripts.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
                    break;
                case GlobalUtils.testExecModeValue2:
                    DockPanelScriptsViewer.Visibility = Visibility.Hidden;
                    DataGridScriptsViewer.Visibility = Visibility.Visible;
                    ScrollViewerTestScripts.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
                    break;
                default:
                    ConfigAdapter.SetValue(GlobalUtils.testExecModeKey, GlobalUtils.testExecModeValue1);
                    goto case GlobalUtils.testExecModeValue1;
            }
        }

        public void ApplyScriptsFilter()
        {
            string testModule = GlobalUtils.testModule;
            if (testModule.Equals("ALL"))
            {
                testModule = "";
            }
            Type[] AllProjectFiles = Assembly.GetExecutingAssembly().GetTypes();
            FilteredFiles = AllProjectFiles.Where(x => (x.FullName.Contains(GlobalUtils.testScenarioFolderName) && x.Name.StartsWith(testModule))).ToArray().OrderBy(item => item.Name).ToArray();
            TbxScriptsFilter.Text = GlobalUtils.scriptsFilter;
            DisplayScripts(FilteredFiles);
        }

        public void DisplayScripts(Type[] filteredFiles)
        {
            try
            {
                int num;
                Type[] filteredFilesTemp = new Type[] { };

                if (GlobalUtils.scriptsFilter.ToUpper().Equals("ALL"))
                {
                    FilteredFilesCurrent = FilteredFiles;
                }
                else if (GlobalUtils.scriptsFilter.Contains(",") || GlobalUtils.scriptsFilter.Contains("-") || int.TryParse(GlobalUtils.scriptsFilter, out num))
                {
                    string[] scriptName;
                    char[] scriptNameDelim = { '-' };
                    string[] scriptNameSplit = GlobalUtils.scriptsFilter.ToUpper().Split(',');
                    int oldTo = 1;
                    int to = 1;
                    int from = 1;

                    if (Regex.Matches(scriptNameSplit[0], @"[a-zA-Z]").Count > 0)
                    {
                        FilteredFilesCurrent = FilteredFiles.Where(c => scriptNameSplit.Any(c.Name.ToUpper().Contains)).ToArray();
                    }
                    else
                    {
                        for (int foo = 0; foo < scriptNameSplit.Length; foo++)
                        {
                            if (Regex.Matches(scriptNameSplit[0], @"[a-zA-Z]").Count > 0)
                            {

                            }

                            scriptName = scriptNameSplit[foo].Split('-');
                            oldTo = to;

                            if (scriptName.Length.Equals(2))
                            {
                                from = int.Parse(scriptName[0].Trim());
                                to = int.Parse(scriptName[1].Trim());
                            }
                            else
                            {
                                from = int.Parse(scriptName[0].Trim());
                                to = int.Parse(scriptName[0].Trim());
                            }

                            filteredFilesTemp = filteredFilesTemp.Concat((filteredFiles.Skip(from - 1).Take(to - from + 1)).ToArray()).ToArray();
                        }

                        FilteredFilesCurrent = (Type[])filteredFilesTemp.Clone();
                    }
                }
                else
                {
                    FilteredFilesCurrent = FilteredFiles.Where(c => c.Name.ToUpper().Contains(GlobalUtils.scriptsFilter.ToUpper())).ToArray();
                }

                DataGridScriptsViewer.Items.Clear();
                TblockScriptsViewer_SetText("");

                for (int foo = 0; foo < FilteredFilesCurrent.Length; foo++)
                {
                    int bar = Array.FindIndex(FilteredFiles, item => item.Name == FilteredFilesCurrent[foo].Name);
                    TblockScriptsViewerNum.Inlines.Add(string.Format("{0}. {1}", (bar + 1), Environment.NewLine));
                    TblockScriptsViewer.Inlines.Add(string.Format("{0}{1}", FilteredFilesCurrent[foo].Name, Environment.NewLine));

                    #region Data Grid: ScriptProperties
                    DataGridScriptsViewer.Items.Add(new ScriptProperties
                    {
                        Number = string.Format("{0}.", (bar + 1).ToString()),
                        Percentage = 0,
                        TestName = FilteredFilesCurrent[foo].Name
                    });
                    #endregion
                }

                if (FilteredFilesCurrent.Length < 1)
                {
                    BtnStart.IsEnabled = false;
                }
                else
                {
                    BtnStart.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public List<ProgressBar> GetPrgBarTestPercentage()
        {
            List<ProgressBar> progressbarList = new List<ProgressBar>();

            try
            {
                ProgressBarTransition.FindChildGroup(DataGridScriptsViewer, "PrgBarTestPercentage", ref progressbarList);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }

            return progressbarList;
        }
        //======================================================================TEST SCRIPTS FILTER=====================================================================

        //=======================================================================SETUP AND TEARDOWN=====================================================================
        public void GuiSetup()
        {
            try
            {
                MenuToolsSettings.IsEnabled = false;
                MenuToolsCustomize.IsEnabled = false;
                BtnPause.IsEnabled = true;
                BtnStart.Content = GlobalUtils.btnLblStop;
                LblTestMethod_SetText("");

                #region GUI: Main Display Panel Update
                if (GlobalUtils.testExecMode.Equals(GlobalUtils.testExecModeValue1))
                {
                    GlobalUtils.InvokeUI(() => TblockScriptsViewer_SetText(""));
                }
                else
                {
                    GlobalUtils.InvokeUI(() => DataGridScriptsViewer_ResetProgress());
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void GuiTeardown(string option)
        {
            try
            {
                MenuToolsSettings.IsEnabled = true;
                MenuToolsCustomize.IsEnabled = true;
                BtnPause.IsEnabled = false;
                BtnPause.Content = "Pause";
                BtnStart.Content = "Start";

                switch (option)
                {
                    case GlobalUtils.lblStatusReady:
                        Window_UpdateStatus(string.Format("{0} {1}", GlobalUtils.lblStatus, GlobalUtils.lblStatusReady));
                        LblTestMethod_SetText("");
                        break;
                    case GlobalUtils.lblStatusStopped:
                        if (GlobalUtils.testExecMode.Equals(GlobalUtils.testExecModeValue1))
                        {
                            GlobalUtils.InvokeUI(() => TblockScriptsViewer_UpdateProgress(GlobalUtils.progressUpdate2, GlobalUtils.brushLoops));
                        }
                        Window_UpdateStatus(string.Format("{0} {1}", GlobalUtils.lblStatus, GlobalUtils.lblStatusStopped));
                        break;
                    case GlobalUtils.lblStatusCompleted:
                        Window_UpdateStatus(string.Format("{0} {1}", GlobalUtils.lblStatus, GlobalUtils.lblStatusCompleted));
                        LblTestMethod_SetText("");
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void DirsSetup()
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(GlobalUtils.resultsPath));

                FileAdapter.CopyFiles(GlobalUtils.resultsPath, string.Format("{0}{1}\\", GlobalUtils.backupPath, GlobalUtils.uniqueNum));
                FileAdapter.DeleteFiles(GlobalUtils.resultsPath);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void DriverSetup()
        {
            try
            {
                WebDriver.Driver = WebDriver.GetDriver(int.Parse(GlobalUtils.webBrowser));
                WebDriver.Builder = new Actions(WebDriver.Driver);
                WebDriver.Wait = new WebDriverWait(WebDriver.Driver, TimeSpan.FromSeconds(WebDriver.globalTimeout));
                WebDriver.ParentWindow = WebDriver.GetWindowHandle();

                #region Interrupt Flag: Interrupts on click of Pause / Stop buttons
                GlobalUtils.InterruptFlag.WaitOne();
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        private void DriverTeardown()
        {
            try
            {
                WebDriver.Quit();
                EndProcess(GlobalUtils.iWebDriver);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //=======================================================================SETUP AND TEARDOWN=====================================================================
    }
}
