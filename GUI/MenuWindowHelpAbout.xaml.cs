﻿using Corvo.Common;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Windows;
using System.Windows.Input;

namespace Corvo.GUI
{
    public partial class MenuWindowHelpAbout : Window
    {
        GlobalLogger Logger = new GlobalLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public MenuWindowHelpAbout(int x, int y)
        {
            InitializeComponent();
            this.Left = x - this.Width / 2;
            this.Top = y - this.Height / 2;

            Window_DisplayAppInfo();
            Window_DisplayProject();
        }
        //=======================================================================WINDOW CONTROLS========================================================================
        private void Window_DisplayAppInfo()
        {
            LblAppInfo.Content = "Application: " + GlobalUtils.version;
        }

        private void Window_DisplayProject()
        {
            LblProject.Content = "Project: " + GlobalUtils.projectName;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key.Equals(Key.Escape))
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //=======================================================================WINDOW CONTROLS========================================================================

        //========================================================================CONTROL BUTTONS=======================================================================
        private void BtnMenuWindowHelpAboutClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //========================================================================CONTROL BUTTONS=======================================================================

        //=============================================================================LINKS============================================================================
        private void LblChangeLog_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(GlobalUtils.changeLogPath);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //=============================================================================LINKS============================================================================
    }
}
