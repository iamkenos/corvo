﻿using Corvo.Common;
using Corvo.GUI.CustomControls;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Corvo.GUI
{
    public partial class MenuWindowToolsSettings : Window
    {
        GlobalLogger Logger = new GlobalLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public MenuWindowToolsSettings(int x, int y)
        {
            InitializeComponent();
            this.Left = x - this.Width / 2;
            this.Top = y - this.Height / 2;

            CbxModule_LoadValues(CbxModule, new RoutedEventArgs());
            CbxBrowser_LoadValues(CbxBrowser, new RoutedEventArgs());
            CbxEnvironment_LoadValues(CbxEnvironment, new RoutedEventArgs());
            RdBtnLoopsType_LoadValues(RdBtnLoops1, new RoutedEventArgs(), GlobalUtils.loopsTypeValue1);
            RdBtnLoopsType_LoadValues(RdBtnLoops2, new RoutedEventArgs(), GlobalUtils.loopsTypeValue2);
            Window_Load();
        }

        //=======================================================================WINDOW CONTROLS========================================================================
        private void Window_Load()
        {
            try
            {
                //---MODULE
                CbxModule.Text = GlobalUtils.testModule;
                //---ENVIRONMENT
                CbxEnvironment.Text = GlobalUtils.testEnvironment;
                //---BROWSER
                CbxBrowser.SelectedIndex = (int.Parse(GlobalUtils.webBrowser) - 1);
                //---SCHEDULER
                TbxScheduleRun.Text = GlobalUtils.scheduleRun;
                TbxScheduleRun.IsEnabled = Convert.ToBoolean(GlobalUtils.scheduleRunEnabled);
                ChbxScheduleRun.IsChecked = Convert.ToBoolean(GlobalUtils.scheduleRunEnabled);
                LblScheduleRunError.Visibility = Visibility.Hidden;
                BtnMenuWindowToolsSettingsSave.IsEnabled = true;
                //---LOOPS
                GetLoopsType();
                TbxLoops.Text = GlobalUtils.loops;
                TbxLoops.IsEnabled = Convert.ToBoolean(GlobalUtils.loopsEnabled);
                ChbxLoops.IsChecked = Convert.ToBoolean(GlobalUtils.loopsEnabled);
                RdBtnLoops1.IsEnabled = Convert.ToBoolean(GlobalUtils.loopsEnabled);
                RdBtnLoops2.IsEnabled = Convert.ToBoolean(GlobalUtils.loopsEnabled);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key.Equals(Key.Escape))
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //=======================================================================WINDOW CONTROLS========================================================================

        //=========================================================================CHECK BOXES==========================================================================
        private void ChbxScheduleRun_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ChbxScheduleRun.IsChecked.Equals(true))
                {
                    TbxScheduleRun.IsEnabled = true;
                    LblScheduleRunError.Visibility = Visibility.Visible;
                    BtnMenuWindowToolsSettingsSave.IsEnabled = TbxScheduleRun_VerifyValue(this, e);
                }
                else
                {
                    TbxScheduleRun.IsEnabled = false;
                    BtnMenuWindowToolsSettingsSave.IsEnabled = true;
                    LblScheduleRunError.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void ChbxLoops_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ChbxLoops.IsChecked.Equals(true))
                {
                    TbxLoops.IsEnabled = true;
                    RdBtnLoops1.IsEnabled = true;
                    RdBtnLoops2.IsEnabled = true;
                }
                else
                {
                    TbxLoops.IsEnabled = false;
                    RdBtnLoops1.IsEnabled = false;
                    RdBtnLoops2.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //=========================================================================CHECK BOXES==========================================================================

        //==========================================================================TEXT BOXES==========================================================================
        private void TbxScheduleRun_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                TbxScheduleRun_VerifyValue(this, e);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private bool TbxScheduleRun_VerifyValue(object sender, RoutedEventArgs e)
        {
            try
            {
                return TextboxMasking.TbxVerifyValue(TbxScheduleRun, LblScheduleRunError, BtnMenuWindowToolsSettingsSave, @"^([0-1]\d|2[0-3])(:)([0-5]\d)(:)([0-5]\d)$");
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }
        //==========================================================================TEXT BOXES==========================================================================

        //========================================================================CONTROL BUTTONS=======================================================================
        private void BtnMenuWindowToolsSettingsSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ConfigAdapter.SetValue(GlobalUtils.testModuleKey, CbxModule.Text);
                ConfigAdapter.SetValue(GlobalUtils.testEnvironmentKey, CbxEnvironment.Text);
                ConfigAdapter.SetValue(GlobalUtils.webBrowserKey, (CbxBrowser.SelectedIndex + 1).ToString());
                ConfigAdapter.SetValue(GlobalUtils.webBrowserNameKey, CbxBrowser.Text);
                ConfigAdapter.SetValue(GlobalUtils.scheduleRunKey, TbxScheduleRun.Text);
                ConfigAdapter.SetValue(GlobalUtils.loopsKey, TbxLoops.Text);
                ConfigAdapter.SetValue(GlobalUtils.scheduleRunEnabledKey, ChbxScheduleRun.IsChecked.ToString());
                ConfigAdapter.SetValue(GlobalUtils.loopsEnabledKey, ChbxLoops.IsChecked.ToString());
                SetLoopsType();

                #region Scheduled Run: Scheduler Setup
                if (ChbxScheduleRun.IsChecked.Equals(true))
                {
                    DateTime scheduleTime = DateTime.Parse(DateTime.Now.ToShortDateString() + " " + TbxScheduleRun.Text);
                    DateTime currentTime = DateTime.Now;
                    int remainingTime = (int)((scheduleTime - currentTime).TotalMilliseconds);

                    if (remainingTime < 0)
                    {
                        scheduleTime = DateTime.Parse(DateTime.Now.AddDays(1).ToShortDateString() + " " + GlobalUtils.scheduleRun);
                    }

                    MainWindow.MainAppWindow.Scheduler = new GlobalScheduler();
                    MainWindow.MainAppWindow.Scheduler.TimerSetup(scheduleTime);
                }
                else
                {
                    if (MainWindow.MainAppWindow.Scheduler != null)
                    {
                        MainWindow.MainAppWindow.Scheduler.TimerCancel();
                    }
                }
                #endregion

                MainWindow.MainAppWindow.ApplyScriptsFilter();
                MainWindow.MainAppWindow.Window_UpdateSettingsPanel();
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void BtnMenuWindowToolsSettingsCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //========================================================================CONTROL BUTTONS=======================================================================

        //==========================================================================COMBO BOXES=========================================================================
        private void CbxModule_LoadValues(object sender, RoutedEventArgs e)
        {
            try
            {
                GlobalUtils.testModuleList.Sort();
                ComboBox cbxModule = sender as ComboBox;
                cbxModule.ItemsSource = GlobalUtils.testModuleList;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void CbxEnvironment_LoadValues(object sender, RoutedEventArgs e)
        {
            try
            {
                ComboBox cbxEnvironment = sender as ComboBox;
                cbxEnvironment.ItemsSource = GlobalUtils.environmentList;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void CbxBrowser_LoadValues(object sender, RoutedEventArgs e)
        {
            try
            {
                ComboBox cbxBrowser = sender as ComboBox;
                cbxBrowser.ItemsSource = GlobalUtils.webBrowserList;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //==========================================================================COMBO BOXES=========================================================================

        //=============================================================================LABELS===========================================================================
        private void LblModule_Click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                CbxModule.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void LblEnvironment_Click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                CbxEnvironment.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void LblBrowser_Click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                CbxBrowser.IsDropDownOpen = true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void LblScheduleRun_Click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                TbxScheduleRun.Focus();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void LblLoops_Click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                TbxLoops.Focus();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //=============================================================================LABELS===========================================================================

        //=========================================================================RADIO BUTTONS========================================================================
        private void RdBtnLoopsType_LoadValues(object sender, RoutedEventArgs e, string value)
        {
            try
            {
                RadioButton rdBtnLoops = sender as RadioButton;
                rdBtnLoops.Content = value;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void GetLoopsType()
        {
            switch (GlobalUtils.loopsType)
            {
                case GlobalUtils.loopsTypeValue1:
                    RdBtnLoops1.IsChecked = true;
                    break;
                case GlobalUtils.loopsTypeValue2:
                    RdBtnLoops2.IsChecked = true;
                    break;
                default:
                    goto case GlobalUtils.loopsTypeValue1;
            }
        }

        private void SetLoopsType()
        {
            if (RdBtnLoops1.IsChecked.Equals(true))
            {
                ConfigAdapter.SetValue(GlobalUtils.loopsTypeKey, GlobalUtils.loopsTypeValue1);
            }
            else
            {
                ConfigAdapter.SetValue(GlobalUtils.loopsTypeKey, GlobalUtils.loopsTypeValue2);
            }
        }
        //=========================================================================RADIO BUTTONS========================================================================
    }
}
