﻿using Corvo.Common;
using System;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Input;

namespace Corvo.GUI
{
    public partial class MenuWindowWarning : Window
    {
        GlobalLogger Logger = new GlobalLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public MenuWindowWarning(int x, int y)
        {
            InitializeComponent();
            this.Left = x - this.Width / 2;
            this.Top = y - this.Height / 2;

            Window_DisplayWarning();
        }

        //=======================================================================WINDOW CONTROLS========================================================================
        private void Window_DisplayWarning()
        {
            try
            {
                string backup = Directory.GetParent(GlobalUtils.backupRootPath).Name + "\\";
                string results = Directory.GetParent(GlobalUtils.resultsRootPath).Name + "\\";
                string logs = Directory.GetParent(GlobalUtils.logsPath).Name + "\\";

                TblockAffectedDirs.Inlines.Add("1. " + backup + Environment.NewLine);
                TblockAffectedDirs.Inlines.Add("2. " + results + Environment.NewLine);
                TblockAffectedDirs.Inlines.Add("3. " + logs + Environment.NewLine + Environment.NewLine);

                TblockAffectedDirs.Inlines.Add("Please confirm to proceed.");
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key.Equals(Key.Escape))
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //=======================================================================WINDOW CONTROLS========================================================================

        //========================================================================CONTROL BUTTONS=======================================================================
        private void BtnMenuWindowWarningConfirm_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FileAdapter.DeleteFiles(GlobalUtils.backupRootPath);
                FileAdapter.DeleteFiles(GlobalUtils.resultsRootPath);
                FileAdapter.DeleteFiles(GlobalUtils.logsPath);
                MainWindow.MainAppWindow.LblTestMethod_SetText(GlobalUtils.lblTestMethodCleanupSuccess);
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void BtnMenuWindowWarningCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //========================================================================CONTROL BUTTONS=======================================================================
    }
}
