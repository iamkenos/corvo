﻿using Corvo.GUI;
using Corvo.GUI.CustomControls;
using Corvo.GUI.Objects;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Corvo.Common
{
    /// <summary>
    /// User for running tests:
    ///     - Test scenario class files
    ///     - Procedure class files
    /// </summary>
    class GlobalRunner
    {
        static GlobalLogger Logger = new GlobalLogger(MethodBase.GetCurrentMethod().DeclaringType);
        static int totalMethods;
        public static string number;
        public static string testName;
        public static double percentage;

        /// <summary>
        /// Used to run test scenario class files. Invoked mainly by the main process test thread
        /// </summary>
        /// <param name="type">Scenario class file to run</param>
        /// 
        public static void Run(Type type, int scriptIndex)
        {
            try
            {
                object instance = Activator.CreateInstance(type);
                MethodInfo[] methodInfos = ConvertArray(type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly));
                if (methodInfos != null)
                {
                    RunMethods(methodInfos, instance, type, scriptIndex);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        /// <summary>
        /// Used to run procedure class files. Invoked mainly on test scenario class files.
        /// When used, this method will invoke all underlying methods on the procedure class sequentially from top to bottom
        /// Sample usage:
        /// 1. Create a new instance of a procedure class file
        ///     > ProcPage1 procPage1 = new ProcPage1();
        /// 2. Invoke the Run method
        ///     > GlobalRunner.Run(procPage1);
        /// </summary>
        /// <param name="instance"></param>
        public static void Run(object instance)
        {
            try
            {
                MethodInfo[] methodInfos = ConvertArray(instance.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly));
                if (methodInfos != null)
                {
                    RunMethods(methodInfos, instance, instance.GetType());
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        //========================================================================RUNNER UTILITIES======================================================================
        /* This section is only used by the application thread.
         * Rightfully, the methods below should not be invoked from any test scenario class files.*/
        private static void RunMethods(MethodInfo[] methodInfos, object instance, Type type, int scriptIndex = 0)
        {
            for (int foo = 0; foo < methodInfos.Length; foo++)
            {
                if (GlobalUtils.RunFlag)
                {
                    string className = type.Name;
                    string methodName = methodInfos[foo].Name;

                    try
                    {
                        #region Interrupt Flag: Interrupts on click of Pause / Stop buttons
                        GlobalUtils.InterruptFlag.WaitOne();
                        #endregion

                        #region GUI: Update LblTestMethod with the current method running
                        if (type.FullName.Contains(GlobalUtils.testScenarioFolderName))
                        {
                            if (GlobalUtils.testExecMode.Equals(GlobalUtils.testExecModeValue2))
                            {
                                ScriptProperties scripts = (ScriptProperties)MainWindow.MainAppWindow.DataGridScriptsViewer.Items[scriptIndex];
                                totalMethods = methodInfos.Length;

                                number = scripts.Number;
                                testName = scripts.TestName;
                                percentage = (double)(foo + 1) / (double)totalMethods * 100;
                            }

                            GlobalUtils.InvokeUI(() => MainWindow.MainAppWindow.LblTestMethod_SetText(string.Format("{0}:{1}", className, methodName)));
                        }
                        else
                        {
                            GlobalUtils.InvokeUI(() => MainWindow.MainAppWindow.LblTestMethod_ShowRunningClass(className, methodName));
                        }
                        #endregion

                        Logger.Info(string.Format("Running Class: {0} Method: {1} ", instance, methodName));
                        methodInfos[foo].Invoke(instance, null);

                        #region GUI: Update Percentage
                        if (GlobalUtils.testExecMode.Equals(GlobalUtils.testExecModeValue2))
                        {
                            if (type.FullName.Contains(GlobalUtils.testScenarioFolderName))
                            {
                                GlobalUtils.InvokeUI(() => MainWindow.MainAppWindow.DataGridScriptsViewer_SetProgress(scriptIndex));
                            }
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                        throw;
                    }
                }
                else
                {
                    break;
                }
            }
        }

        private static MethodInfo[] ConvertArray(MethodInfo[] methodInfos)
        {
            try
            {
                if (methodInfos.Length.Equals(0))
                {
                    return null;
                }
                if (!methodInfos[0].Name.Equals("FirstFunction"))
                {
                    int foo = methodInfos.Length / 64;
                    MethodInfo[] newMethodInfos = new MethodInfo[methodInfos.Length]; ;

                    for (int bar = 1; bar <= foo + 1; bar++)
                    {
                        Array.Copy(methodInfos, GetSourceStart(methodInfos.Length, bar), newMethodInfos, 64 * (bar - 1), GetDestLength(methodInfos.Length, bar));
                    }
                    return newMethodInfos;
                }
                else
                {
                    return methodInfos;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return null;
            }
        }

        private static int GetSourceStart(int total, int foo)
        {
            int SourceStart = total - 64 * foo;
            if (SourceStart < 0)
            {
                return 0;
            }
            else
            {
                return SourceStart;
            }
        }

        private static int GetDestLength(int total, int foo)
        {
            int bar = total / 64;
            if (foo <= bar)
            {
                return 64;
            }
            else
            {
                return total - 64 * (foo - 1);
            }
        }
        //========================================================================RUNNER UTILITIES======================================================================
    }
}
