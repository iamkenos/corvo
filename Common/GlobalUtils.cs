﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;

namespace Corvo.Common
{
    /// <summary>
    /// Contains global statics and constants.
    /// Common, non-project specific variables and methods that can and will be used globally should fall on this class.
    /// </summary>
    public class GlobalUtils
    {
        //========================================================================PROCESS THREAD========================================================================
        public static Thread ProcessThread;
        public static ManualResetEvent InterruptFlag;
        public static volatile bool RunFlag;

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetCurrentMethodName()
        {
            StackTrace stackTrace = new StackTrace();
            StackFrame stackFrame = stackTrace.GetFrame(1);

            return stackFrame.GetMethod().Name;
        }

        public static void InvokeUI(Action action)
        {
            Application.Current.Dispatcher.Invoke(action);
        }
        //========================================================================PROCESS THREAD========================================================================

        //==========================================================================APP CONFIG==========================================================================
        #region Constants: App.config Keys
        public readonly static string testModuleKey = "testModule";
        public readonly static string testEnvironmentKey = "testEnvironment";
        public readonly static string webBrowserKey = "webBrowser";
        public readonly static string webBrowserNameKey = "webBrowserName";
        public readonly static string scheduleRunEnabledKey = "scheduleRunEnabled";
        public readonly static string scheduleRunKey = "scheduleRun";
        public readonly static string loopsEnabledKey = "loopsEnabled";
        public readonly static string loopsKey = "loops";
        public readonly static string loopsTypeKey = "loopsType";
        public readonly static string testExecModeKey = "testExecMode";
        public readonly static string concurrentTestsKey = "concurrentTests";
        public readonly static string globalTimeoutKey = "globalTimeout";
        public readonly static string versionKey = "version";
        public readonly static string projectNameKey = "projectName";
        public readonly static string currentIterKey = "currentIter";
        public readonly static string scriptsFilterKey = "scriptsFilter";
        public readonly static string testTimeStampStartKey = "testTimeStampStart";
        public readonly static string testTimeStampEndKey = "testTimeStampEnd";
        public readonly static string loopedRunKey = "loopedRun";
        public readonly static string iWebDriverKey = "iWebDriver";
        #endregion
        //==========================================================================APP CONFIG==========================================================================

        //============================================================================COMMON============================================================================ 
        #region Constants: Test Data File 
        public const string testDataActionType1 = "Read";
        public const string testDataActionType2 = "Write";
        public readonly static string testDataPrefix = "TD_";
        public readonly static string testDataPath = System.IO.Directory.GetCurrentDirectory() + "\\Resources\\Data\\";
        #endregion

        #region Constants: Screenshot File
        public const string screenShotType1 = "Image";
        public const string screenShotType2 = "HTML";
        public readonly static ImageFormat screenShotFileType = ImageFormat.Png;
        public readonly static string screenHtmlFileExt = ".htm";
        public readonly static string screenShotFileExt = ".png";
        public readonly static string screenShotFolderName = "Screenshots\\";
        #endregion

        #region Constants: Results File
        public readonly static string commonDateFormat = "dd-MM-yyyy";
        public readonly static string resultsFile = "Results";
        public readonly static string resultsTextFile = resultsFile + ".txt";
        public readonly static string resultsHtmlFile = resultsFile + ".htm";
        public readonly static string resultsZipExt = ".zip";
        public readonly static string resultsZipFile = resultsFile + resultsZipExt;
        public readonly static char resultsFileDelimiter = ',';
        #endregion

        #region Constants: IWebDriver Process Names
        public readonly static string iWebDriverChrome = "chromedriver";
        public readonly static string iWebDriverPJs = "phantomjs";
        public readonly static string iWebDriverIE = "IEDriverServer";
        public readonly static string iWebDriverEdge = "MicrosoftWebDriver";
        #endregion

        #region Constants: File Locations 
        public static string testScenarioFolderName = "TestScenarios";
        public static string viewHelpPath = System.IO.Directory.GetCurrentDirectory() + "\\Resources\\Documentation\\Corvo Automation Framework - Documentation.pdf";
        public static string changeLogPath = System.IO.Directory.GetCurrentDirectory() + "\\Resources\\Documentation\\ChangeLog.txt";
        public static string logsPath = System.IO.Directory.GetCurrentDirectory() + "\\Logs\\";
        public static string backupRootPath = System.IO.Directory.GetCurrentDirectory() + "\\Backup\\";
        public static string resultsRootPath = System.IO.Directory.GetCurrentDirectory() + "\\" + resultsFile + "\\";
        public static string chromeDownloadPath = System.IO.Directory.GetCurrentDirectory() + "\\Resources\\Drivers\\Downloads\\";
        public static string chromeDriverPath = System.IO.Directory.GetCurrentDirectory() + "\\Resources\\Drivers\\";
        public static string geckoDriverPath = System.IO.Directory.GetCurrentDirectory() + "\\Resources\\Drivers\\";
        public static string firefoxCustomBinPath = System.IO.Directory.GetCurrentDirectory() + "\\Resources\\Drivers\\Firefox\\FirefoxPortable.exe";
        public static string ieDriverPath = System.IO.Directory.GetCurrentDirectory() + "\\Resources\\Drivers\\";
        public static string edgeDriverPath = System.IO.Directory.GetCurrentDirectory() + "\\Resources\\Drivers\\";
        public static string phantomDriverPath = System.IO.Directory.GetCurrentDirectory() + "\\Resources\\Drivers\\";

        public static string backupPath
        {
            get
            {
                return backupRootPath + DateTime.Now.ToString(commonDateFormat) + "\\" + testEnvironment + "\\" + testModule + "\\";
            }
        }

        public static string resultsPath
        {
            get
            {
                return resultsRootPath + DateTime.Now.ToString(commonDateFormat) + "\\" + testEnvironment + "\\" + testModule + "\\";
            }
        }

        public static string resultsTxtPath
        {
            get
            {
                return resultsPath + resultsTextFile;
            }
        }

        public static string resultsHtmlPath
        {
            get
            {
                return resultsPath + resultsHtmlFile;
            }
        }

        public static string resultsOverallPath
        {
            get
            {
                return resultsRootPath + DateTime.Now.ToString(commonDateFormat) + "\\" + testEnvironment + "\\";
            }
        }

        public static string resultsOverallHtmlPath
        {
            get
            {
                return resultsOverallPath + resultsHtmlFile;
            }
        }

        public static string screenShotPath
        {
            get
            {
                return resultsPath + screenShotFolderName + currentIter + "\\";
            }
        }
        #endregion

        public static string uniqueNum
        {
            get
            {
                return DateTime.Now.ToString("HHmmss");
            }
        }

        public static int randomNum
        {
            get
            {
                Random r = new Random();
                return r.Next(1000, 9999);
            }
        }

        public static string iWebDriver
        {
            get
            {
                return ConfigAdapter.GetValue(iWebDriverKey);
            }
        }
        //============================================================================COMMON============================================================================

        //==========================================================================MAIN WINDOW=========================================================================
        #region Constants: Main Display Box Test Results 
        public readonly static string iconPassed = "\u2713";
        public readonly static string iconFailed = "\u2717";
        public readonly static string iconBlocked = "\u23F5";
        public readonly static string brushPassed = "#004D00";
        public readonly static string brushFailed = "#800000";
        public readonly static string brushLoops = "#595959";
        public readonly static string brushNewTest = "#B38300";
        public readonly static string lineBreak = "----------------------------------------------------------------------";
        public readonly static string lblTestMethodExecutionCompleted = "Notice: Execution completed.";
        public readonly static string lblTestMethodReportError = "Notice: Results file is not available.";
        public readonly static string lblTestMethodReportSuccess = "Notice: Results saved at ";
        public readonly static string lblTestMethodCleanupSuccess = "Notice: Clean up successful.";
        public const string progressUpdate1 = "Begin";
        public const string progressUpdate2 = "Stop";
        public const string progressUpdate3 = "Complete";
        public const string progressUpdate4 = "CompleteLoops";
        #endregion

        #region Constants: Left Pane
        public const string lblDisabled = "Disabled";
        public const string lblModule = "Module";
        public const string lblEnvironment = "Environment";
        public const string lblBrowser = "Browser";
        public const string lblScheduler = "Scheduler";
        public const string lblLoops = "Loops";
        public const string lblStatus = "Status";
        public const string lblStatusRunning = "Running...";
        public const string lblStatusReady = "Ready";
        public const string lblStatusPaused = "Paused";
        public const string lblStatusStopped = "Stopped";
        public const string lblStatusCompleted = "Completed";
        #endregion

        #region Constants: Button Labels
        public const string btnLblStart = "Start";
        public const string btnLblStop = "Stop";
        public const string btnLblPause = "Pause";
        public const string btnLblResume = "Resume";
        #endregion

        public static string scriptsFilter
        {
            get
            {
                return ConfigAdapter.GetValue(scriptsFilterKey);
            }
        }
        //==========================================================================MAIN WINDOW=========================================================================

        //=============================================================================REPORT===========================================================================
        #region Constants: HTML Report Styles
        public readonly static int tableResultsWidth = 1100;
        public readonly static string fontHeader = "Verdana";
        public readonly static string fontContent = "Verdana";
        public readonly static string fontFamilyBody = "Verdana,sans-serif";
        public readonly static string fontSizeBody = "11px";
        public readonly static string fontSizeSummary = "12px";
        public readonly static string fontSizeSubHeader = "16px";
        public readonly static string fontSizeHeader = "26px";
        public readonly static string colorHeader = "#1A1A1A";
        public readonly static string colorSubHeader = "#1A1A1A";
        public readonly static string colorContent = "#000000";
        public readonly static string tableHeaderBGColor = "#333333";
        public readonly static string tableHeaderTextColor = "#FFFFFF";
        public readonly static string tableHighlightColor = "#BFBFBF";
        public readonly static string tableRowColor1 = "#FFFFFF";
        public readonly static string tableRowColor2 = "#EEEEEE";
        public readonly static string styleBody = "body {font: " + fontSizeBody + " " + fontFamilyBody + ";margin-left: 70px;margin-right: 70px;}table {margin-top: 5px;}";
        public readonly static string styleHeader = ".header {width: " + tableResultsWidth + "px;border-style: none!important;font-size: " + fontSizeHeader + "!important; font-weight: bold; color: " + colorHeader + "; text-align: center; white-space: nowrap;}";
        public readonly static string styleSubHeader = ".subheader {width: " + tableResultsWidth + "px;border-style: none!important;font-size: " + fontSizeSubHeader + "!important; font-weight: bold; color: " + colorSubHeader + ";}";
        public readonly static string styleSummary = ".summary {width: " + tableResultsWidth + "px;border-style: none!important;font-size: " + fontSizeSubHeader + "!important; font-weight: bold; color: " + colorSubHeader + ";}";
        public readonly static string styleSummaryHeader = ".summary-header {width: 140px;px;border-style: none!important;font-size: " + fontSizeSubHeader + "!important; font-weight: bold; color: " + colorSubHeader + ";}";
        public readonly static string styleSummaryData = ".summary-data {border-style: none!important;font-size: " + fontSizeSummary + "!important; font-weight: bold; color: " + colorContent + ";}";
        public readonly static string styleTdTr = "td,tr[style] {font-size: 11px!important;border-width: 1px!important;border-style: solid!important;border-color: " + colorContent + " !important;}";

        public static string htmlFormatter
        {
            get
            {
                return string.Format("<style>{0} {1} {2} {3} {4} {5} {6}</style>", styleBody, styleHeader, styleSubHeader, styleSummary, styleSummaryHeader, styleSummaryData, styleTdTr);
            }
        }
        #endregion

        #region Constants: HTML Report Labels
        public readonly static string tableLabelPassed = "Passed";
        public readonly static string tableLabelFailed = "Failed";
        public readonly static string tableLabelPassedRate = "Pass Rate";
        public readonly static string tableLabelTCBlocked = "BLOCKED";

        public readonly static string tsTblTestScenario = "Test Scenario";
        public readonly static string tsTblResult = "Result";
        public readonly static string tsTblTotalTC = "TCs Executed";
        public readonly static string tsTblPassed = tableLabelPassed;
        public readonly static string tsTblFailed = tableLabelFailed;
        public readonly static string tsTblPassRate = tableLabelPassedRate;

        public readonly static string tcTblIteration = "Iteration";
        public readonly static string tcTblTestScenario = "Test Scenario";
        public readonly static string tcTblTestCase = "Test Case";
        public readonly static string tcTblDescription = "Description";
        public readonly static string tcTblResult = "Result";
        public readonly static string tcTblTimeStamp = "Time Stamp";

        public readonly static string modTblTestModule = "Module";
        public readonly static string modTblResult = "Result";
        public readonly static string modTblTotalTC = "TCs Executed";
        public readonly static string modTblPassed = tableLabelPassed;
        public readonly static string modTblFailed = tableLabelFailed;
        public readonly static string modTblPassRate = tableLabelPassedRate;
        #endregion

        public static string currentIter
        {
            get
            {
                return ConfigAdapter.GetValue(currentIterKey);
            }
        }

        public static string testTimeStampStart
        {
            get
            {
                return ConfigAdapter.GetValue(testTimeStampStartKey);
            }
        }

        public static string testTimeStampEnd
        {
            get
            {
                return ConfigAdapter.GetValue(testTimeStampEndKey);
            }
        }

        public static string loopedRun
        {
            get
            {
                return ConfigAdapter.GetValue(loopedRunKey);
            }
        }
        //=============================================================================REPORT===========================================================================

        //===========================================================================SETTINGS===========================================================================
        #region Values: Dropdown Lists & Radio Options
        public static List<string> testModuleList = new List<string>(new string[] {
            "ALL",
            "FLIGHTS",
            "HOME"});

        public static List<string> environmentList = new List<string>(new string[] {
            "SIT",
            "UAT",
            "PRE-PROD" });

        public static List<string> webBrowserList = new List<string>(new string[] {
            "Google Chrome",
            "Mozilla Firefox",
            "Internet Explorer",
            "Phantom JS" });

        public const string loopsTypeValue1 = "Standard";
        public const string loopsTypeValue2 = "Data Driven";
        #endregion

        public static string testModule
        {
            get
            {
                return ConfigAdapter.GetValue(testModuleKey);
            }
        }

        public static string testEnvironment
        {
            get
            {
                return ConfigAdapter.GetValue(testEnvironmentKey);
            }
        }

        public static string webBrowser
        {
            get
            {
                return ConfigAdapter.GetValue(webBrowserKey);
            }
        }

        public static string webBrowserName
        {
            get
            {
                return ConfigAdapter.GetValue(webBrowserNameKey);
            }
        }

        public static string scheduleRun
        {
            get
            {
                return ConfigAdapter.GetValue(scheduleRunKey);
            }
        }

        public static string scheduleRunEnabled
        {
            get
            {
                return ConfigAdapter.GetValue(scheduleRunEnabledKey);
            }
        }

        public static string loops
        {
            get
            {
                return ConfigAdapter.GetValue(loopsKey);
            }
        }

        public static string loopsEnabled
        {
            get
            {
                return ConfigAdapter.GetValue(loopsEnabledKey);
            }
        }

        public static string loopsType
        {
            get
            {
                return ConfigAdapter.GetValue(loopsTypeKey);
            }
        }
        //===========================================================================SETTINGS===========================================================================

        //===========================================================================CUSTOMIZE==========================================================================
        #region Values: Radio Options
        public const string testExecModeValue1 = "Single";
        public const string testExecModeValue2 = "Concurrent";
        #endregion

        public static string testExecMode
        {
            get
            {
                return ConfigAdapter.GetValue(testExecModeKey);
            }
        }

        public static string concurrentTests
        {
            get
            {
                return ConfigAdapter.GetValue(concurrentTestsKey);
            }
        }

        public static string globalTimeout
        {
            get
            {
                return ConfigAdapter.GetValue(globalTimeoutKey);
            }
        }
        //===========================================================================CUSTOMIZE==========================================================================

        //=============================================================================ABOUT============================================================================
        #region Constants: Assembly Information; Do not change.
        public const string assemblyTitle = "Corvo";
        public const string assemblyVersion = "1.3.*";
        #endregion

        public static string version
        {
            get
            {
                return ConfigAdapter.GetValue(versionKey);
            }
        }

        public static string projectName
        {
            get
            {
                return ConfigAdapter.GetValue(projectNameKey);
            }
        }
        //=============================================================================ABOUT============================================================================
    }
}
