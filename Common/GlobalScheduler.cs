﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using Corvo.GUI;
using System.Reflection;

namespace Corvo.Common
{
    /// <summary>
    /// Used to set and trigger a scheduled run for the main process test thread
    /// </summary>
    public class GlobalScheduler : IDisposable
    {
        Timer timer;
        GlobalLogger Logger = new GlobalLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public GlobalScheduler()
        {
            timer = new Timer(x =>
            {
                SchedulerTriggerProcess();
            }, null, Timeout.Infinite, Timeout.Infinite);
        }

        public void TimerSetup(DateTime alertTime)
        {
            try
            {
                DateTime current = DateTime.Now;
                int timeToGo = (int)((alertTime - current).TotalMilliseconds);
                if (timeToGo <= 0)
                {
                    timeToGo = (int)((alertTime.AddDays(1) - current).TotalMilliseconds);
                }
                timer.Change(timeToGo, Timeout.Infinite);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public void TimerCancel()
        {
            try
            {
                timer.Change(Timeout.Infinite, Timeout.Infinite);
                timer.Dispose();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        private void SchedulerTriggerProcess()
        {
            try
            {
                MainWindow.MainAppWindow.StartProcess();
                timer.Dispose();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                timer.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
