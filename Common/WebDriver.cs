﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace Corvo.Common
{
    /// <summary>
    /// Custom wrapper of the selenium IWebDriver class. Used to interact with the IWebDriver instance
    /// Has 6 main sections:
    ///     Scenario & Procedure level methods:
    ///         - ACTIONS
    ///         - GET
    ///         - VERIFICATIONS
    ///         - WAIT AND INTERACT
    ///         - RANDOMIZE
    ///     Process level methods:
    ///         - UTILS
    /// </summary>
    class WebDriver
    {
        public static IWebDriver Driver;
        public static Actions Builder;
        public static WebDriverWait Wait;
        public static string ParentWindow;
        public static int globalTimeout = int.Parse(GlobalUtils.globalTimeout);

        private static GlobalLogger Logger = new GlobalLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static string chromeDownloadPath = GlobalUtils.chromeDownloadPath;
        private static string chromeDriverPath = GlobalUtils.chromeDriverPath;
        private static string geckoDriverPath = GlobalUtils.geckoDriverPath;
        private static string firefoxCustomBinPath = GlobalUtils.firefoxCustomBinPath;
        private static string ieDriverPath = GlobalUtils.ieDriverPath;
        private static string edgeDriverPath = GlobalUtils.edgeDriverPath;
        private static string phantomDriverPath = GlobalUtils.phantomDriverPath;

        public static IWebDriver GetDriver(int webBrowser)
        {
            try
            {
                switch (webBrowser)
                {
                    #region Google Chrome
                    case 1:
                        Logger.Info(">>>>Starting Chrome Driver>>>>");

                        ChromeDriverService chromeDriverService = ChromeDriverService.CreateDefaultService(chromeDriverPath);
                        DesiredCapabilities chromeCapabilities = DesiredCapabilities.Chrome();
                        ChromeOptions chromeOptions = new ChromeOptions();
                        Proxy chromeProxy = new Proxy();

                        chromeOptions.AddUserProfilePreference("download.default_directory", chromeDownloadPath);
                        chromeOptions.AddUserProfilePreference("download.prompt_for_download", false);
                        chromeOptions.AddLocalStatePreference("profile.default_content_settings.popups", 0);
                        chromeOptions.AddArguments("--disable-extensions");
                        chromeOptions.AddArguments("--allow-running-insecure-content");
                        chromeCapabilities.SetCapability(ChromeOptions.Capability, chromeOptions);
                        chromeCapabilities.SetCapability("proxy", chromeProxy);
                        chromeProxy.NoProxy = null;

                        chromeDriverService.HideCommandPromptWindow = true;
                        chromeDriverService.SuppressInitialDiagnosticInformation = true;

                        ChromeDriver driverChrome = new ChromeDriver(chromeDriverService, chromeOptions, TimeSpan.FromSeconds(globalTimeout));
                        driverChrome.Manage().Window.Maximize();

                        ConfigAdapter.SetValue(GlobalUtils.iWebDriverKey, GlobalUtils.iWebDriverChrome);
                        return driverChrome;
                    #endregion

                    #region Mozilla Firefox
                    case 2:
                        Logger.Info(">>>>Starting Firefox Driver>>>>");

                        FirefoxDriverService ffDriverService = FirefoxDriverService.CreateDefaultService(geckoDriverPath);
                        DesiredCapabilities ffCapabilities = DesiredCapabilities.Firefox();
                        FirefoxOptions ffOptions = new FirefoxOptions();
                        ffOptions.AddArguments("--disable-extensions");
                        ffOptions.AddArguments("--allow-running-insecure-content");

                        ffDriverService.HideCommandPromptWindow = true;
                        ffDriverService.SuppressInitialDiagnosticInformation = true;

                        FirefoxDriver driverFF = new FirefoxDriver(ffDriverService, ffOptions, TimeSpan.FromSeconds(globalTimeout));
                        driverFF.Manage().Window.Maximize();
                        return driverFF;
                    #endregion

                    #region Internet Explorer
                    case 3:
                        Logger.Info(">>>>Starting IE Driver>>>>");
                        InternetExplorerDriverService ieDriverService = InternetExplorerDriverService.CreateDefaultService(ieDriverPath);
                        InternetExplorerOptions ieOptions = new InternetExplorerOptions();

                        ieDriverService.HideCommandPromptWindow = true;
                        ieDriverService.SuppressInitialDiagnosticInformation = true;

                        InternetExplorerDriver driverIE = new InternetExplorerDriver(ieDriverService, ieOptions, TimeSpan.FromSeconds(globalTimeout));
                        driverIE.Manage().Window.Maximize();

                        ConfigAdapter.SetValue(GlobalUtils.iWebDriverKey, GlobalUtils.iWebDriverIE);
                        return driverIE;
                    #endregion

                    #region PhantomJS
                    case 4:
                        Logger.Info(">>>>Starting PhantomJS Driver>>>>");
                        PhantomJSDriverService pjsDriverService = PhantomJSDriverService.CreateDefaultService(phantomDriverPath);
                        PhantomJSOptions pjsOptions = new PhantomJSOptions();

                        pjsDriverService.HideCommandPromptWindow = true;
                        pjsDriverService.SuppressInitialDiagnosticInformation = true;

                        PhantomJSDriver driverPJs = new PhantomJSDriver(pjsDriverService, pjsOptions, TimeSpan.FromSeconds(globalTimeout));

                        ConfigAdapter.SetValue(GlobalUtils.iWebDriverKey, GlobalUtils.iWebDriverPJs);
                        return driverPJs;
                    #endregion

                    #region Edge
                    case 5:
                        Logger.Info(">>>>Starting Microsoft Edge Driver>>>>");
                        EdgeDriverService edgeDriverService = EdgeDriverService.CreateDefaultService(edgeDriverPath);
                        EdgeOptions edgeOptions = new EdgeOptions();

                        edgeDriverService.HideCommandPromptWindow = true;
                        edgeDriverService.SuppressInitialDiagnosticInformation = true;

                        EdgeDriver driverEdge = new EdgeDriver(edgeDriverService, edgeOptions);
                        driverEdge.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(globalTimeout));

                        ConfigAdapter.SetValue(GlobalUtils.iWebDriverKey, GlobalUtils.iWebDriverEdge);
                        return driverEdge;
                        #endregion
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        //============================================================================ACTIONS===========================================================================
        public static void OpenURL(string URL)
        {
            try
            {
                #region Interrupt Flag: Interrupt on click of Pause / Stop buttons
                GlobalUtils.InterruptFlag.WaitOne();
                #endregion
                Driver.Navigate().GoToUrl(URL);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void AlertAccept()
        {
            try
            {
                SwitchToAlert().Accept();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void AlertDismiss()
        {
            try
            {
                SwitchToAlert().Dismiss();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void ClickElementByJS(By by)
        {
            try
            {
                ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].click();", FindElement(by));
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void ClickElementByJS(IWebElement element)
        {
            try
            {
                ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].click();", element);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void ClickElement(By by)
        {
            try
            {
                Wait.Until(ExpectedConditions.ElementToBeClickable(by));
                FindElement(by).Click();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void ClickElement(IWebElement elem)
        {
            try
            {
                Wait.Until(ExpectedConditions.ElementToBeClickable(elem));
                elem.Click();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void DropdownSendKeys(By by, string text)
        {
            try
            {
                FindElement(by).SendKeys(text);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void DropdownSendKeys(IWebElement elem, string text)
        {
            try
            {
                elem.SendKeys(text);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void DropdownSelect(By by, int optIndex)
        {
            try
            {
                SelectElement dropdown = new SelectElement(Driver.FindElement(by));
                dropdown.SelectByIndex(optIndex);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void DropdownSelect(IWebElement elem, int optIndex)
        {
            try
            {
                SelectElement dropdown = new SelectElement(elem);
                dropdown.SelectByIndex(optIndex);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void DropdownSelect(By by, string optText)
        {
            try
            {
                SelectElement dropdown = new SelectElement(Driver.FindElement(by));
                dropdown.SelectByText(optText);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void DropdownSelect(IWebElement elem, string optText)
        {
            try
            {
                SelectElement dropdown = new SelectElement(elem);
                dropdown.SelectByText(optText);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void RefreshPage()
        {
            try
            {
                Driver.Navigate().Refresh();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void SendKeysAndWait(By by, string text)
        {
            try
            {
                FindElement(by).Clear();
                FindElement(by).SendKeys(text);
                Wait.Until(d => d.FindElement(by).GetAttribute("value").Equals(text));
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void SendKeysAndWait(IWebElement elem, string text)
        {
            try
            {
                elem.Clear();
                elem.SendKeys(text);
                Wait.Until(d => elem.GetAttribute("value").Equals(text));
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void SendKeysByJSAndWait(By by, string text)
        {
            try
            {
                string js = "arguments[0].value='" + text + "';";
                ((IJavaScriptExecutor)Driver).ExecuteScript(js, FindElement(by));
                Wait.Until(d => d.FindElement(by).GetAttribute("value").Equals(text));
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void SendKeysByJSAndWait(IWebElement elem, string text)
        {
            try
            {
                string js = "arguments[0].value='" + text + "';";
                ((IJavaScriptExecutor)Driver).ExecuteScript(js, elem);
                Wait.Until(d => elem.GetAttribute("value").Equals(text));
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void SwitchToNewWindow(string windowText)
        {
            IWebDriver newWindow = null;
            List<string> windows = GetWindowHandles();

            while (windows.Count < 2)
            {
                windows = GetWindowHandles();
            }

            try
            {
                foreach (var window in windows)
                {
                    newWindow = Driver.SwitchTo().Window(window);
                    if (newWindow.PageSource.Contains(windowText))
                    {
                        newWindow.Manage().Window.Maximize();
                        return;
                    }
                }
                SwitchToParentWindow(ParentWindow);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void SwitchToParentWindow(string parentHandle)
        {
            try
            {
                Driver.SwitchTo().Window(parentHandle);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static IAlert SwitchToAlert()
        {
            try
            {
                Stopwatch stWatch = Stopwatch.StartNew();
                double elapsedS = stWatch.Elapsed.TotalSeconds;

                while (elapsedS < globalTimeout)
                {
                    if (VerifyAlertPresent())
                    {
                        return Driver.SwitchTo().Alert();
                    }
                    elapsedS = stWatch.Elapsed.TotalSeconds;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }
        //============================================================================ACTIONS===========================================================================

        //==============================================================================GET=============================================================================
        public static string GetURL()
        {
            try
            {
                return Driver.Url;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static string GetTitle()
        {
            try
            {
                return Driver.Title;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static string GetElementValue(By by)
        {
            try
            {
                return FindElement(by).GetAttribute("value");
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static string GetElementValue(IWebElement elem)
        {
            try
            {
                return elem.GetAttribute("value");
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static string GetSelectedDropdownText(By by)
        {
            try
            {
                return (string)((IJavaScriptExecutor)Driver).ExecuteScript("return arguments[0].options[arguments[0].selectedIndex].text;", FindElement(by));
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static string GetSelectedDropdownText(IWebElement elem)
        {
            try
            {
                return (string)((IJavaScriptExecutor)Driver).ExecuteScript("return arguments[0].options[arguments[0].selectedIndex].text;", elem);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static string GetSelectedDropdownValue(By by)
        {
            try
            {
                return (string)((IJavaScriptExecutor)Driver).ExecuteScript("return arguments[0].options[arguments[0].selectedIndex].value;", FindElement(by));
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static string GetSelectedDropdownValue(IWebElement elem)
        {
            try
            {
                return (string)((IJavaScriptExecutor)Driver).ExecuteScript("return arguments[0].options[arguments[0].selectedIndex].value;", elem);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static string GetSelectedDropdownIndex(By by)
        {
            try
            {
                return (string)((IJavaScriptExecutor)Driver).ExecuteScript("return arguments[0].selectedIndex;", FindElement(by));
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static string GetSelectedDropdownIndex(IWebElement elem)
        {
            try
            {
                return (string)((IJavaScriptExecutor)Driver).ExecuteScript("return arguments[0].selectedIndex;", elem);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static string GetText(By by)
        {
            try
            {
                return FindElement(by).Text;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static string GetText(IWebElement elem)
        {
            try
            {
                return elem.Text;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static string GetAttribute(By by, string attribute)
        {
            try
            {
                return FindElement(by).GetAttribute(attribute);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static string GetAttribute(IWebElement elem, string attribute)
        {
            try
            {
                return elem.GetAttribute(attribute);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static string GetWindowHandle()
        {
            try
            {
                return Driver.CurrentWindowHandle;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static List<string> GetWindowHandles()
        {
            try
            {
                List<string> handlesList = new List<string>();
                ReadOnlyCollection<string> handles = Driver.WindowHandles;

                foreach (string handle in handles)
                {
                    handlesList.Add(handle);
                }

                return handlesList;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }
        //==============================================================================GET=============================================================================

        //=========================================================================VERIFICATIONS========================================================================
        public static bool VerifyAlertPresent()
        {
            try
            {
                Driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyElementContainsText(By by, string text)
        {
            try
            {
                return GetText(by).Contains(text);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyElementContainsText(IWebElement elem, string text)
        {
            try
            {
                return GetText(elem).Contains(text);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyElementText(By by, string text)
        {
            try
            {
                return GetText(by).Equals(text);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyElementText(IWebElement elem, string text)
        {
            try
            {
                return GetText(elem).Equals(text);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyElementPresent(By by)
        {
            try
            {
                WaitUntilElemExists(by);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyElementPresent(IWebElement elem)
        {
            try
            {
                WaitUntilElemExists(elem);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyElementDisplayed(By by)
        {
            try
            {
                WaitUntilElemIsDisplayed(by);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyElementDisplayed(IWebElement elem)
        {
            try
            {
                WaitUntilElemIsDisplayed(elem);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyElementEnabled(By by)
        {
            try
            {
                WaitUntilElemIsEnabled(by);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyElementEnabled(IWebElement elem)
        {
            try
            {
                WaitUntilElemIsEnabled(elem);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyElementDisabled(By by)
        {
            try
            {
                WaitUntilElemIsDisabled(by);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyElementDisabled(IWebElement elem)
        {
            try
            {
                WaitUntilElemIsDisabled(elem);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyFieldIsPopulated(By by)
        {
            try
            {
                WaitUntilFieldIsPopulated(by);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyFieldIsPopulated(IWebElement elem)
        {
            try
            {
                WaitUntilFieldIsPopulated(elem);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyElemSelected(By by)
        {
            try
            {
                WaitUntilElemSelected(by);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyElemSelected(IWebElement elem)
        {
            try
            {
                WaitUntilElemSelected(elem);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyElemNotSelected(By by)
        {
            try
            {
                WaitUntilElemNotSelected(by);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyElemNotSelected(IWebElement elem)
        {
            try
            {
                WaitUntilElemNotSelected(elem);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                return false;
            }
        }

        public static bool VerifyPageURL(string url)
        {
            return Driver.Url.Equals(url);
        }

        //=========================================================================VERIFICATIONS========================================================================

        //=======================================================================WAIT AND INTERACT======================================================================
        public static void WaitExplicit(int seconds)
        {
            try
            {
                Thread.Sleep(seconds * 1000);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void WaitUntilElemExists(By by)
        {
            try
            {
                Wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(by));
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void WaitUntilElemExists(IWebElement elem)
        {
            try
            {
                Stopwatch stWatch = Stopwatch.StartNew();
                double elapsedS = stWatch.Elapsed.TotalSeconds;

                while (elapsedS < globalTimeout)
                {
                    if (elem.Displayed)
                    {
                        return;
                    }
                    elapsedS = stWatch.Elapsed.TotalSeconds;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void WaitUntilElemIsDisplayed(By by)
        {
            try
            {
                Wait.Until(d => FindElement(by).Displayed);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void WaitUntilElemIsDisplayed(IWebElement elem)
        {
            try
            {
                Wait.Until(d => elem.Displayed);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void WaitUntilElemIsEnabled(By by)
        {
            try
            {
                Wait.Until(d => FindElement(by).Enabled);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void WaitUntilElemIsEnabled(IWebElement elem)
        {
            try
            {
                Wait.Until(d => elem.Enabled);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void WaitUntilElemIsDisabled(By by)
        {
            try
            {
                Wait.Until(d => !FindElement(by).Enabled);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void WaitUntilElemIsDisabled(IWebElement elem)
        {
            try
            {
                Wait.Until(d => !elem.Enabled);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void WaitUntilFieldIsPopulated(By by)
        {
            try
            {
                Wait.Until(d => FindElement(by).GetAttribute("value").Length > 0);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void WaitUntilFieldIsPopulated(IWebElement elem)
        {
            try
            {
                Wait.Until(d => elem.GetAttribute("value").Length > 0);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void WaitUntilElemSelected(By by)
        {
            try
            {
                Wait.Until(d => FindElement(by).Selected);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void WaitUntilElemSelected(IWebElement element)
        {
            try
            {
                Wait.Until(d => element.Selected);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void WaitUntilElemNotSelected(By by)
        {
            try
            {
                Wait.Until(d => !FindElement(by).Selected);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void WaitUntilElemNotSelected(IWebElement element)
        {
            try
            {
                Wait.Until(d => !element.Selected);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //=======================================================================WAIT AND INTERACT======================================================================

        //===========================================================================RANDOMIZE==========================================================================
        public static string EnterTextAlphabetic(By by, string value, int min = minRange, int max = maxRange)
        {
            if (value.Length.Equals(0))
            {
                Random rnd = new Random();
                value = RandomAlphabet(rnd.Next(min, max));
            }
            SendKeysAndWait(by, value);
            return value;
        }

        public static string EnterTextAlphabetic(IWebElement elem, string value, int min = minRange, int max = maxRange)
        {
            if (value.Length.Equals(0))
            {
                Random rnd = new Random();
                value = RandomAlphabet(rnd.Next(min, max));
            }
            SendKeysAndWait(elem, value);
            return value;
        }

        public static string EnterTextAlphanumeric(By by, string value, int min = minRange, int max = maxRange)
        {
            if (value.Length.Equals(0))
            {
                Random rnd = new Random();
                value = RandomAlphaNum(rnd.Next(min, max));
            }
            SendKeysAndWait(by, value);
            return value;
        }

        public static string EnterTextAlphanumeric(IWebElement elem, string value, int min = minRange, int max = maxRange)
        {
            if (value.Length.Equals(0))
            {
                Random rnd = new Random();
                value = RandomAlphaNum(rnd.Next(min, max));
            }
            SendKeysAndWait(elem, value);
            return value;
        }

        public static string EnterTextNumeric(By by, string value, int min = minRange, int max = maxRange)
        {
            if (value.Length.Equals(0))
            {
                Random rnd = new Random();
                value = RandomNumber(rnd.Next(min, max));
            }
            SendKeysAndWait(by, value);
            return value;
        }

        public static string EnterTextNumeric(IWebElement elem, string value, int min = minRange, int max = maxRange)
        {
            if (value.Length.Equals(0))
            {
                Random rnd = new Random();
                value = RandomNumber(rnd.Next(min, max));
            }
            SendKeysAndWait(elem, value);
            return value;
        }

        public static string EnterTextEmail(By by, string value, int min = minRange, int max = maxRange)
        {
            if (value.Length.Equals(0))
            {
                Random rnd = new Random();
                value = RandomAlphaNum(rnd.Next(min, max));
            }
            SendKeysAndWait(by, value);
            return value + emailPrefix;
        }

        public static string EnterTextEmail(IWebElement elem, string value, int min = minRange, int max = maxRange)
        {
            if (value.Length.Equals(0))
            {
                Random rnd = new Random();
                value = RandomAlphaNum(rnd.Next(min, max));
                value = value + emailPrefix;
            }

            SendKeysAndWait(elem, value);
            return value;
        }

        public static string SelectOption(By by, int index = 0)
        {
            IWebElement elem = FindElement(by);
            string xpath = GetXPath(elem) + "/option";
            int totalOptions = FindElements(By.XPath(xpath)).Count;
            if (index.Equals(0))
            {
                index = RandomIndex(totalOptions);
            }
            DropdownSelect(elem, index);
            return GetText(By.XPath(xpath + "[" + (index + 1) + "]")).Trim();
        }

        public static string SelectOption(IWebElement elem, int index = 0)
        {
            string xpath = GetXPath(elem) + "/option";
            int totalOptions = FindElements(By.XPath(xpath)).Count;
            if (index.Equals(0))
            {
                index = RandomIndex(totalOptions);
            }
            DropdownSelect(elem, index);
            return GetText(By.XPath(xpath + "[" + (index + 1) + "]")).Trim();
        }

        public static string SelectRadio(By by)
        {
            string selection;
            ClickElement(by);

            selection = GetText(by).Trim();
            return selection;
        }

        public static string SelectRadio(IWebElement elem)
        {
            string selection;
            ClickElement(elem);

            selection = GetText(elem).Trim();
            return selection;
        }

        public static string SelectRadio(ReadOnlyCollection<IWebElement> elems)
        {
            string selection;
            int totalOptions = elems.Count;
            IWebElement elem = elems[RandomIndex(totalOptions)];
            ClickElement(elem);

            selection = GetText(elem).Trim();
            return selection;
        }

        #region Randomization: Parameters
        const int minRange = 3;
        const int maxRange = 12;
        const string emailPrefix = "@autoemail.com";
        const string getXpathJS = "function absoluteXPath(element) {" +
                    "var comp, comps = [];" +
                    "var parent = null;" +
                    "var xpath = '';" +
                    "var getPos = function(element) {" +
                    "var position = 1, curNode;" +
                    "if (element.nodeType == Node.ATTRIBUTE_NODE) {" +
                    "return null;" +
                    "}" +
                    "for (curNode = element.previousSibling; curNode; curNode = curNode.previousSibling) {" +
                    "if (curNode.nodeName == element.nodeName) {" +
                    "++position;" +
                    "}" + "}" +
                    "return position;" +
                    "};" +
                    "if (element instanceof Document) {" +
                    "return '/';" +
                    "}" +
                    "for (; element && !(element instanceof Document); element = element.nodeType == Node.ATTRIBUTE_NODE ? element.ownerElement : element.parentNode) {" +
                    "comp = comps[comps.length] = {};" + "switch (element.nodeType) {" +
                    "case Node.TEXT_NODE:" +
                    "comp.name = 'text()';" +
                    "break;" +
                    "case Node.ATTRIBUTE_NODE:" +
                    "comp.name = '@' + element.nodeName;" +
                    "break;" +
                    "case Node.PROCESSING_INSTRUCTION_NODE:" +
                    "comp.name = 'processing-instruction()';" +
                    "break;" +
                    "case Node.COMMENT_NODE:" +
                    "comp.name = 'comment()';" +
                    "break;" +
                    "case Node.ELEMENT_NODE:" +
                    "comp.name = element.nodeName;" +
                    "break;" + "}" +
                    "comp.position = getPos(element);" + "}" +
                    "for (var i = comps.length - 1; i >= 0; i--) {" +
                    "comp = comps[i];" +
                    "xpath += '/' + comp.name.toLowerCase();" +
                    "if (comp.position !== null) {" +
                    "xpath += '[' + comp.position + ']';" + "}" + "}" + "return xpath;" + "} return absoluteXPath(arguments[0]);";
        #endregion

        #region Randomization: Private Functions
        private static int RandomIndex(int totalOptions, int startIndex = 0)
        {
            Random r = new Random();
            return r.Next(startIndex, totalOptions);
        }

        private static string RandomAlphabet(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            Random random = new Random();
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private static string RandomAlphaNum(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            Random random = new Random();
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private static string RandomNumber(int length)
        {
            const string chars = "1234567890";
            Random random = new Random();
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private static string GetXPath(By by)
        {
            return (string)((IJavaScriptExecutor)Driver).ExecuteScript(getXpathJS, FindElement(by));
        }

        private static string GetXPath(IWebElement elem)
        {
            return (string)((IJavaScriptExecutor)Driver).ExecuteScript(getXpathJS, elem);
        }
        #endregion
        //===========================================================================RANDOMIZE==========================================================================

        //=============================================================================UTILS============================================================================
        public static void Quit()
        {
            try
            {
                Driver.Quit();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static IWebElement FindElement(By by)
        {
            IWebElement elem = null;
            try
            {
                #region Interrupt Flag: Interrupt on click of Pause / Stop buttons
                GlobalUtils.InterruptFlag.WaitOne();
                #endregion
                elem = Driver.FindElement(by);
                Builder.MoveToElement(elem).Perform();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
            return elem;
        }

        public static ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            ReadOnlyCollection<IWebElement> elems = null;
            try
            {
                #region Interrupt Flag: Interrupt on click of Pause / Stop buttons
                GlobalUtils.InterruptFlag.WaitOne();
                #endregion

                elems = Driver.FindElements(by);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
            return elems;
        }

        public static string TakeScreenshot([System.Runtime.CompilerServices.CallerFilePath] string testScenario = "", string fileName = "")
        {
            string screenShotPath = null;

            try
            {
                screenShotPath = TakeScreenshotUtil(GlobalUtils.screenShotType1, testScenario, fileName);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }

            return screenShotPath;
        }

        public static string TakeHTMLScreenshot([System.Runtime.CompilerServices.CallerFilePath] string testScenario = "", string fileName = "")
        {
            string screenShotPath = null;

            try
            {
                screenShotPath = TakeScreenshotUtil(GlobalUtils.screenShotType2, testScenario, fileName);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }

            return screenShotPath;
        }

        private static string TakeScreenshotUtil(string screenShotType, string testScenario, string fileName)
        {
            #region Relative Path: Initialize relative path variable
            string screenShotPath = null;
            string fileExt = null;
            #endregion

            try
            {
                #region Get file extension based on [screenShotType]
                switch (screenShotType)
                {
                    case GlobalUtils.screenShotType1:
                        fileExt = GlobalUtils.screenShotFileExt;
                        break;
                    case GlobalUtils.screenShotType2:
                        fileExt = GlobalUtils.screenHtmlFileExt;
                        break;
                }
                #endregion

                #region Relative Path: Process file location; Take screenshot || HTML source and create file; Return relative path.
                if (testScenario.Contains(".cs"))
                {
                    testScenario = GlobalReporter.GetTestScenarioName(testScenario);
                }

                if (fileName.Equals(""))
                {
                    fileName = GlobalUtils.uniqueNum + GlobalUtils.randomNum + fileExt;
                }
                else
                {
                    fileName = fileExt;
                }

                string outputPath = GlobalUtils.screenShotPath + testScenario + "\\";
                Directory.CreateDirectory(outputPath);
                screenShotPath = outputPath + fileName;

                switch (screenShotType)
                {
                    case GlobalUtils.screenShotType1:
                        ImageFormat imgFormat = GlobalUtils.screenShotFileType;
                        ((ITakesScreenshot)Driver).GetScreenshot().SaveAsFile(outputPath + fileName, imgFormat);
                        break;
                    case GlobalUtils.screenShotType2:
                        FileAdapter.OverwriteFile(screenShotPath, Driver.PageSource);
                        break;
                }

                screenShotPath = screenShotPath.Substring(screenShotPath.LastIndexOf(GlobalUtils.screenShotFolderName));
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }

            return screenShotPath;
        }
        //=============================================================================UTILS============================================================================
    }
}
