﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections;
using System.IO;
using System.Reflection;

namespace Corvo.Common
{
    /// <summary>
    /// Used for file interactions [text, html, folders, logs, zip...]
    ///     - Create
    ///     - Delete
    ///     - Copy
    ///     - Overwrite
    /// </summary>
    class FileAdapter
    {
        static GlobalLogger Logger = new GlobalLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static void AppendFile(string filePath, string content)
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
                using (StreamWriter file = new StreamWriter(filePath, true))
                {
                    file.Write(content);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void OverwriteFile(string filePath, string content)
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
                using (StreamWriter file = new StreamWriter(filePath, false))
                {
                    file.Write(content);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void CopyFile(string filePath, string fileDest)
        {
            try
            {
                File.Copy(filePath, fileDest, true);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void CopyFiles(string sourcePath, string destPath, bool copyDirs = true)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(sourcePath);
                DirectoryInfo[] dirs = dir.GetDirectories();
                FileInfo[] files = dir.GetFiles();

                if (dirs.Length.Equals(0) && files.Length.Equals(0))
                {
                    return;
                }

                if (!Directory.Exists(destPath))
                {
                    Directory.CreateDirectory(destPath);
                }

                foreach (FileInfo file in files)
                {
                    string tempPath = Path.Combine(destPath, file.Name);
                    file.CopyTo(tempPath, true);
                }

                if (copyDirs)
                {
                    foreach (DirectoryInfo subDir in dirs)
                    {
                        string tempPath = Path.Combine(destPath, subDir.Name);
                        CopyFiles(subDir.FullName, tempPath, copyDirs);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void DeleteFile(string filePath)
        {
            try
            {
                File.Delete(filePath);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void DeleteFiles(string sourcePath, bool removeDirs = true)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(sourcePath);
                DirectoryInfo[] dirs = dir.GetDirectories();
                FileInfo[] files = dir.GetFiles();

                foreach (FileInfo file in files)
                {
                    file.Delete();
                }

                if (removeDirs)
                {
                    foreach (DirectoryInfo subDir in dirs)
                    {
                        DeleteFiles(subDir.FullName, removeDirs);
                        subDir.Delete(true);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static void CreateZippedFiles(string sourcePath, string destPathAndFile, string password = "")
        {
            try
            {
                int trimLength = sourcePath.IndexOf(GlobalUtils.resultsFile) + GlobalUtils.resultsFile.Length + 1;
                ArrayList fileArrayList = GetFileListToZip(sourcePath);
                ZipOutputStream zipOutStream = new ZipOutputStream(File.Create(destPathAndFile));

                #region Zip File: Set compression level and password 
                zipOutStream.SetLevel(9);
                if (!password.Equals(""))
                {
                    zipOutStream.Password = password;
                }
                #endregion

                #region Zip Entries: Create a zip entry for each file
                foreach (string file in fileArrayList)
                {
                    ZipEntry zipEntry = new ZipEntry(file.Remove(0, trimLength));
                    zipOutStream.PutNextEntry(zipEntry);

                    if (!file.EndsWith(@"/"))
                    {
                        FileStream fileStream = File.OpenRead(file);
                        byte[] obuffer = new byte[fileStream.Length];
                        fileStream.Read(obuffer, 0, obuffer.Length);
                        zipOutStream.Write(obuffer, 0, obuffer.Length);
                        fileStream.Close();
                    }
                }
                #endregion

                zipOutStream.Finish();
                zipOutStream.Close();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        //=======================================================================ADAPTER UTILITIES======================================================================
        private static ArrayList GetFileListToZip(string dir)
        {
            bool isEmpty = true;
            ArrayList fileArrayList = new ArrayList();

            #region File List: Create list of files to be zipped found under [dir]
            foreach (string file in Directory.GetFiles(dir))
            {
                fileArrayList.Add(file);
                isEmpty = false;
            }

            if (isEmpty)
            {
                if (Directory.GetDirectories(dir).Length.Equals(0))
                {
                    fileArrayList.Add(dir + @"/");
                }
            }
            #endregion

            #region File List: Recursive adding
            foreach (string dirs in Directory.GetDirectories(dir))
            {
                foreach (object obj in GetFileListToZip(dirs))
                {
                    fileArrayList.Add(obj);
                }
            }
            #endregion

            return fileArrayList;
        }
        //=======================================================================ADAPTER UTILITIES======================================================================
    }
}
