﻿using System.Configuration;

namespace Corvo.Common
{
    /// <summary>
    /// Used for App.config file interactions
    ///     - Read value using [key] provided
    ///     - Update value using [key] provided
    /// </summary>
    class ConfigAdapter
    {
        /// <summary>
        /// Update values in App.config file
        /// </summary>
        /// <param name="key">XML key to update</param>
        /// <param name="value">New value for the selected [key]</param>
        public static void SetValue(string key, string value)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings[key].Value = value;
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        /// <summary>
        /// Read values from App.config file
        /// </summary>
        /// <param name="key">XML key to read</param>
        /// <returns>String value of the selected [key]</returns>
        public static string GetValue(string key)
        {
            return ConfigurationManager.AppSettings.Get(key);
        }
    }
}
