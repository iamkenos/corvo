﻿using log4net;
using System;

namespace Corvo.Common
{
    /// <summary>
    /// Custom wrapper of the log4Net ILog class. Used to create logfile entries:
    ///     - DEBUG
    ///     - INFO
    ///     - ERROR
    ///     - FATAL
    ///     - WARNING
    /// </summary>
    class GlobalLogger
    {
        private readonly ILog log4NetAdapter;

        public GlobalLogger(Type type)
        {
            this.log4NetAdapter = LogManager.GetLogger(type);
        }

        public void Debug(string message)
        {
            this.log4NetAdapter.Debug(message);
        }

        public void Debug(string message, Exception exception)
        {
            this.log4NetAdapter.Debug(message, exception);
        }

        public void Error(string message)
        {
            this.log4NetAdapter.Error(message);
        }

        public void Error(string message, Exception exception)
        {
            try
            {
                #region Trim Exception String
                string temp = exception.ToString();
                string trimText1 = ".cs:line";
                string trimText2 = " at ";
                string trimText3 = " ";
                int trimIndex;

                if (temp.Contains(".cs:line"))
                {
                    trimIndex = temp.IndexOf(trimText1) + trimText1.Length + 5;
                }
                else
                {
                    trimIndex = temp.IndexOf(trimText2);
                }

                if (!(trimIndex >= temp.Length))
                {
                    temp = temp.Remove(trimIndex);
                }

                while (temp.EndsWith(trimText3))
                {
                    temp = temp.TrimEnd();
                }
                #endregion
                this.log4NetAdapter.Error(string.Format("{0} {1}", message, temp));
            }
            catch
            {
                this.log4NetAdapter.Error(message, exception);
            }
        }

        public void Fatal(string message)
        {
            this.log4NetAdapter.Fatal(message);
        }

        public void Fatal(string message, Exception exception)
        {
            this.log4NetAdapter.Fatal(message, exception);
        }

        public void Info(string message)
        {
            this.log4NetAdapter.Info(message);
        }

        public void Info(string message, Exception exception)
        {
            this.log4NetAdapter.Info(message, exception);
        }

        public void Warning(string message)
        {
            this.log4NetAdapter.Warn(message);
        }

        public void Warning(string message, Exception exception)
        {
            this.log4NetAdapter.Warn(message, exception);
        }
    }
}
