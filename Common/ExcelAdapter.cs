﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace Corvo.Common
{
    /// <summary>
    /// Excel file interactions
    ///     - Get the sheet using the provided [tdSheet]
    ///     - Read value using the provided [tdLabel] 
    ///     - Update value using the provided [tdValue] 
    /// </summary>
    class ExcelAdapter
    {
        static GlobalLogger Logger = new GlobalLogger(MethodBase.GetCurrentMethod().DeclaringType);
        static string fileName;
        static string fullFileName;
        static string testDataPath = GlobalUtils.testDataPath;
        static string testDataPrefix = GlobalUtils.testDataPrefix;
        const string actionType1 = GlobalUtils.testDataActionType1;
        const string actionType2 = GlobalUtils.testDataActionType2;
        static IWorkbook xlWorkbook;
        static ISheet xlSheet;

        public static string ReadData(string tdSheet, string tdLabel)
        {
            string data = null;
            try
            {
                data = RunAdapterUtil(actionType1, tdSheet, tdLabel);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
            return data;
        }

        public static void UpdateData(string tdSheet, string tdLabel, string tdValue)
        {
            try
            {
                RunAdapterUtil(actionType2, tdSheet, tdLabel, tdValue);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }
        //=======================================================================ADAPTER UTILITIES======================================================================
        private static string OpenXLFile(string fileName, string exSheet)
        {
            string fullFileName = null;

            retry:
            try
            {
                DirectoryInfo dir = new DirectoryInfo(GlobalUtils.testDataPath);
                FileInfo[] files = dir.GetFiles();

                foreach (FileInfo file in files)
                {
                    if (file.Name.Contains(fileName))
                    {
                        fileName = file.Name;
                        fullFileName = file.FullName;

                        using (FileStream xlFile = new FileStream(fullFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                        {
                            if (fileName.EndsWith(".xlsx"))
                            {
                                xlWorkbook = new XSSFWorkbook(xlFile);

                            }
                            else if (fileName.EndsWith(".xls"))
                            {
                                xlWorkbook = new HSSFWorkbook(xlFile);
                            }
                            xlWorkbook.GetCreationHelper().CreateFormulaEvaluator().EvaluateAll();
                            xlSheet = xlWorkbook.GetSheet(exSheet);
                        }
                        break;
                    }
                }
            }
            catch (IOException)
            {
                MessageBox.Show(new Form() { TopMost = true }, "Please close the test data file: " + fileName + " and click on OK.", "File Access Conflict", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                goto retry;
            }
            return fullFileName;
        }

        private static string GetStringValue(ICell cell, CellType cellType)
        {
            try
            {
                switch (cellType)
                {
                    case CellType.String:
                        return cell.RichStringCellValue.String;
                    case CellType.Formula:
                        return GetStringValue(cell, cell.CachedFormulaResultType);
                    case CellType.Numeric:
                        #region Check numeric sub-formats
                        if (DateUtil.IsCellDateFormatted(cell))
                        {
                            return cell.DateCellValue.ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            return cell.NumericCellValue.ToString();
                        }
                    #endregion
                    case CellType.Blank:
                        return "";
                    default:
                        return cell.RichStringCellValue.String;
                }
            }
            catch (InvalidOperationException)
            {
                return cell.ToString();
            }
            catch (NullReferenceException)
            {
                return "";
            }
        }

        private static string RunAdapterUtil(string action, string tdSheet, string tdLabel, string tdValue = null)
        {
            fileName = GlobalUtils.testDataPrefix + GlobalUtils.testEnvironment;
            fullFileName = OpenXLFile(fileName, tdSheet);
            string data = null;
            string label = null;
            int cellDataIndex = 1;
            int dataRange = xlSheet.LastRowNum + 1;

            if (Convert.ToBoolean(GlobalUtils.loopsEnabled) && GlobalUtils.loopsType.Equals(GlobalUtils.loopsTypeValue2))
            {
                cellDataIndex = int.Parse(GlobalUtils.currentIter);
            }

            for (int row = 2; row < dataRange; row++)
            {
                ICell labelCell = xlSheet.GetRow(row).GetCell(0);
                label = GetStringValue(labelCell, labelCell.CellType);

                if (label.Equals(tdLabel))
                {
                    switch (action)
                    {
                        case actionType1:
                            data = ReadDataUtil(row, cellDataIndex);
                            break;
                        case actionType2:
                            UpdateDataUtil(row, cellDataIndex, tdSheet, tdValue);
                            break;
                    }
                    break;
                }
            }
            return data;
        }

        private static string ReadDataUtil(int row, int cellDataIndex)
        {
            ICell dataCell = xlSheet.GetRow(row).GetCell(cellDataIndex);
            return GetStringValue(dataCell, dataCell.CellType);
        }

        private static void UpdateDataUtil(int row, int cellDataIndex, string tdSheet, string tdValue)
        {
            xlSheet.GetRow(row).CreateCell(cellDataIndex);
            xlSheet.GetRow(row).GetCell(cellDataIndex).SetCellValue(tdValue);
            xlSheet.ForceFormulaRecalculation = true;

            retry:
            try
            {
                FileAdapter.DeleteFile(fullFileName);
                using (FileStream file = new FileStream(fullFileName, FileMode.CreateNew, FileAccess.Write, FileShare.ReadWrite))
                {
                    xlWorkbook.Write(file);
                }
            }
            catch (IOException)
            {
                MessageBox.Show(new Form() { TopMost = true }, "Please close the test data file: " + fileName + " and click on OK.", "File Access Conflict", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                goto retry;
            }
        }
        //=======================================================================ADAPTER UTILITIES======================================================================
    }
}
