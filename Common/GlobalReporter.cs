﻿using APIReport;
using Corvo.GUI;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Documents;

namespace Corvo.Common
{
    /// <summary>
    /// Used for generating the HTML report file.
    /// Non-utility methods will be used mainly by test scenario class files.
    /// </summary>
    class GlobalReporter
    {
        static string environmentOverall;
        static string dateOverall;
        static float passedCountOverall;
        static float failedCountOverall;
        static float totalExecOverall;
        static float passedRateOverall;
        static GlobalLogger Logger = new GlobalLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Write test case result to a text file that will be used for HTML generation.
        /// Each call will create 1 test case line record in the report having 1 screenshot link of the current screen the moment this method was invoked.
        /// </summary>
        /// <param name="testCaseResult">
        ///     Boolean true or false. Use WebDriver [Verify] methods to pass verification results here</param>
        /// <param name="testCaseID">
        ///     The case being tested the moment this method was invoked. Use TC[ID] and it will be appended by [testScenario]. 
        ///     Sample: TC01, TC02 ...</param>
        /// <param name="testDescription"></param>
        ///     [Optional] A short description of the case being tested the moment this method was invoked.
        ///     If not supplied, default value will be the CallerMemberName / method name enclosing the line where this method was invoked.
        /// <param name="testScenario"></param>
        ///     [Optional] Recommended to leave this param blank.
        ///     Default value will be CallerFilePath / class name of the calling method enclosing the line where this method was invoked.
        public static void WriteResult(
            bool testCaseResult,
            string testCaseID,
            [System.Runtime.CompilerServices.CallerMemberName]string testDescription = "",
            [System.Runtime.CompilerServices.CallerFilePath] string testScenario = "")
        {
            try
            {
                testCaseID = WriteResultsUtil(GlobalUtils.screenShotType1, testCaseResult, testCaseID, testDescription, testScenario);
                bool isBlocked = testCaseID.Contains(GlobalUtils.tableLabelTCBlocked);

                #region GUI: Main Panel Update
                if (GlobalUtils.testExecMode.Equals(GlobalUtils.testExecModeValue1))
                {
                    GlobalUtils.InvokeUI(() => UpdateGUIScriptsPanel(testCaseResult, testCaseID, isBlocked));
                }
                else
                {
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        /// <summary>
        /// Write test case result to a text file that will be used for HTML generation.
        /// Each call will create 1 test case line record in the report having 1 HTML link of the current screen the moment this method was invoked.
        /// </summary>
        /// <param name="testCaseResult">
        ///     Boolean true or false. Use WebDriver [Verify] methods to pass verification results here</param>
        /// <param name="testCaseID">
        ///     The case being tested the moment this method was invoked. Use TC[ID] and it will be appended by [testScenario]. 
        ///     Sample: TC01, TC02 ...</param>
        /// <param name="testDescription"></param>
        ///     [Optional] A short description of the case being tested the moment this method was invoked.
        ///     If not supplied, default value will be the CallerMemberName / method name enclosing the line where this method was invoked.
        /// <param name="testScenario"></param>
        ///     [Optional] Recommended to leave this param blank.
        ///     Default value will be CallerFilePath / class name of the calling method enclosing the line where this method was invoked.
        public static void WriteResultWithHTMLSnapShot(
            bool testCaseResult,
            string testCaseID,
            [System.Runtime.CompilerServices.CallerMemberName]string testDescription = "",
            [System.Runtime.CompilerServices.CallerFilePath] string testScenario = "")
        {
            try
            {
                testCaseID = WriteResultsUtil(GlobalUtils.screenShotType1, testCaseResult, testCaseID, testDescription, testScenario);

                #region GUI: Main Panel Update
                if (GlobalUtils.testExecMode.Equals(GlobalUtils.testExecModeValue1))
                {
                    GlobalUtils.InvokeUI(() => UpdateGUIScriptsPanel(testCaseResult, testCaseID));
                }
                else
                {
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        /// <summary>
        /// Write test case result to a text file that will be used for HTML generation.
        /// Each call will create 1 test case line record in the report having [N] number of screenshot or HTML link of different screens taken.
        /// </summary>
        /// <param name="testStepResults">
        ///     Dictionay [string-bool] key-value pair.
        ///     The string key should contain the relative path of an image / html screenshot while the bool value should contain a boolean true or false of a WebDriver [Verify] method.
        ///     This method should come hand-in-hand with the following methods:
        ///      - WebDriver.TakeScreenshot
        ///      - WebDriver.TakeHTMLScreenshot
        ///      Sample usage:
        ///      1. Declare a Dictionary object on the test scenario class. 1 test case should correspond to 1 dictionary object
        ///         > Dictionary<string, bool> verificationsTC01 = new Dictionary<string, bool>();
        ///      2. For each verification point on the case being tested, add a key-value pair containing the screenshot / html file relative path AND a verification result
        ///         > verificationsTC01.Add(WebDriver.TakeScreenshot(), WebDriver.VerifyElementPresent(By by));
        ///         > verificationsTC01.Add(WebDriver.TakeScreenshot(), WebDriver.VerifyFieldIsPopulated(By by));
        ///      3. After all the verification points are completed for the case being tested, invoke the method and pass the parameters
        ///         > GlobalReporter.WriteAllStepResults(verificationsTC01, "TC01", "Description 1");
        ///      </param>
        /// <param name="testCaseID">
        ///     The case being tested the moment this method was invoked. Use TC[ID] and it will be appended by [testScenario]. 
        ///     Sample: TC01, TC02 ...</param>
        /// <param name="testDescription"></param>
        ///     [Optional] A short description of the case being tested the moment this method was invoked.
        ///     If not supplied, default value will be the CallerMemberName / method name enclosing the line where this method was invoked.
        /// <param name="testScenario"></param>
        ///     [Optional] Recommended to leave this param blank.
        ///     Default value will be CallerFilePath / class name of the calling method enclosing the line where this method was invoked.
        public static void WriteAllStepResults(
            Dictionary<string, bool> testStepResults,
            string testCaseID,
            [System.Runtime.CompilerServices.CallerMemberName] string testDescription = "",
            [System.Runtime.CompilerServices.CallerFilePath] string testScenario = "")
        {
            try
            {
                string content;
                string stepResults = null;
                bool testCaseResult = GetTestResult(testStepResults.Values.ToArray());

                #region Content: Build content for results text file
                testScenario = GetTestScenarioName(testScenario);
                testCaseID = testScenario + "_" + testCaseID;

                foreach (KeyValuePair<string, bool> foo in testStepResults)
                {
                    stepResults = stepResults + foo.Value.ToString().Replace("True", "<a target='_blank' href=\"" + foo.Key + "\">" + GlobalUtils.tableLabelPassed + "</a><br>").Replace("False", "<a target='_blank' href=\"" + foo.Key + "\">" + GlobalUtils.tableLabelFailed + "</a><br>");
                    Console.WriteLine("{0}, {1}", foo.Key, foo.Value);
                }

                content = SetTextFileContent(testCaseResult, testScenario, testCaseID, testDescription, null, stepResults);
                #endregion

                GlobalUtils.InterruptFlag.WaitOne();

                #region Text File: Write raw text results for HTML
                FileAdapter.AppendFile(GlobalUtils.resultsTxtPath, content);
                #endregion

                GenerateReport();

                #region GUI: Main Panel Update
                if (GlobalUtils.testExecMode.Equals(GlobalUtils.testExecModeValue1))
                {
                    GlobalUtils.InvokeUI(() => UpdateGUIScriptsPanel(testCaseResult, testCaseID));
                }
                else
                {
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        //========================================================================REPORT UTILITIES======================================================================
        /* This section are only used by the application thread.
         * Rightfully, the methods below should not be invoked from any test scenario class files.*/

        public static void GenerateReport()
        {
            try
            {
                DocumentHTML documentHTML = new DocumentHTML();
                StreamWriter streamWriter = new StreamWriter(GlobalUtils.resultsHtmlPath);

                #region HTML: Variables - Report Title
                string header = string.Format("[{0}] Automation Test Report", GlobalUtils.projectName);
                #endregion

                #region HTML: Variables - Summary Section
                int tcDataPassedCount = GetModuleResultSummary()[GlobalUtils.tableLabelPassed];
                int tcDataFailedCount = GetModuleResultSummary()[GlobalUtils.tableLabelFailed];
                #endregion

                #region HTML: Variables - Test Scenario Result Table
                DataTable tsDataTable = GetTSResultData();
                DataView tsDataView = new DataView(tsDataTable);
                int tsDataColumnCount = tsDataTable.Columns.Count;
                int tsDataResultColumn = tsDataTable.Columns.IndexOf(GlobalUtils.tsTblResult);
                int tsDataTotalTCColumn = tsDataTable.Columns.IndexOf(GlobalUtils.tsTblTotalTC);
                int tsDataPassedColumn = tsDataTable.Columns.IndexOf(GlobalUtils.tsTblPassed);
                int tsDataFailedColumn = tsDataTable.Columns.IndexOf(GlobalUtils.tsTblFailed);
                int tsDataPassRateColumn = tsDataTable.Columns.IndexOf(GlobalUtils.tsTblPassRate);
                int[] tsNarrowCols = { tsDataResultColumn, tsDataTotalTCColumn, tsDataPassedColumn, tsDataFailedColumn, tsDataPassRateColumn };
                #endregion

                #region HTML: Variables - Test Case Result Table
                DataTable tcDataTable = GetTCResultData();
                DataTable tcDataCarrier = tcDataTable.DefaultView.ToTable(true, GlobalUtils.tcTblTestScenario);
                int tcDataColumnCount = tcDataTable.Columns.Count;
                int tcDataIterColumn = tcDataTable.Columns.IndexOf(GlobalUtils.tcTblIteration);
                int tcDataDescColumn = tcDataTable.Columns.IndexOf(GlobalUtils.tcTblDescription);
                int tcDataResultColumn = tcDataTable.Columns.IndexOf(GlobalUtils.tcTblResult);
                int[] tcNarrowCols = { tcDataIterColumn, tcDataResultColumn };
                int[] tcWideCols = { tcDataDescColumn };
                #endregion

                #region HTML: Add report title
                documentHTML.addSectionText(CreateReportContent(GlobalUtils.htmlFormatter));
                documentHTML.addSectionText(CreateReportHeader(header));
                documentHTML.addSectionText(InsertNewLine());
                #endregion

                #region HTML: Add report summary
                documentHTML.addSectionText(CreateReportSummary(tcDataPassedCount, tcDataFailedCount));
                documentHTML.addSectionText(InsertNewLine());
                #endregion

                #region HTML: Add Test Scenario Result table
                tsDataView.Sort = GlobalUtils.tsTblTestScenario;
                ReportSectionData rsdTestScenarioResult = new ReportSectionData(tsDataView.ToTable(), false);
                rsdTestScenarioResult.BgColorColumnTitle = SetColors(tsDataColumnCount, GlobalUtils.tableHeaderBGColor);
                rsdTestScenarioResult.ForeColorTitle = SetColors(tsDataColumnCount, GlobalUtils.tableHeaderTextColor);
                rsdTestScenarioResult.Alignment = ContentAlignment.MiddleCenter;
                rsdTestScenarioResult.Position = Position.center;
                rsdTestScenarioResult.TotalWidth = GlobalUtils.tableResultsWidth;
                rsdTestScenarioResult.HighLightColor = ColorTranslator.FromHtml(GlobalUtils.tableHighlightColor);
                rsdTestScenarioResult.ColWidth = SetColumnWidth(tsDataColumnCount, tsNarrowCols);
                rsdTestScenarioResult.setAlternatingRowBgColorAlgo1(ColorTranslator.FromHtml(GlobalUtils.tableRowColor1), ColorTranslator.FromHtml(GlobalUtils.tableRowColor2));
                documentHTML.addSectionData(rsdTestScenarioResult);
                documentHTML.addSectionText(InsertNewLine());
                documentHTML.addSectionText(InsertNewLine());
                documentHTML.addSectionText(InsertNewLine());
                #endregion

                #region HTML: Add Test Case Result tables. Tables will be separated by [tcDataCarrier]
                for (int i = 0; i < tcDataCarrier.Rows.Count; i++)
                {
                    DataView tcDataView = new DataView(tcDataTable);
                    tcDataView.RowFilter = "[" + GlobalUtils.tcTblTestScenario + "]='" + tcDataCarrier.Rows[i][GlobalUtils.tcTblTestScenario].ToString() + "'";
                    ReportSectionData rsdTestCaseResult = new ReportSectionData(tcDataView.ToTable(), false);
                    rsdTestCaseResult.BgColorColumnTitle = SetColors(tcDataColumnCount, GlobalUtils.tableHeaderBGColor);
                    rsdTestCaseResult.ForeColorTitle = SetColors(tcDataColumnCount, GlobalUtils.tableHeaderTextColor);
                    rsdTestCaseResult.Alignment = ContentAlignment.MiddleCenter;
                    rsdTestCaseResult.Position = Position.center;
                    rsdTestCaseResult.TotalWidth = GlobalUtils.tableResultsWidth;
                    rsdTestCaseResult.HighLightColor = ColorTranslator.FromHtml(GlobalUtils.tableHighlightColor);
                    rsdTestCaseResult.ColWidth = SetColumnWidth(tcDataColumnCount, tcNarrowCols, tcWideCols);
                    rsdTestCaseResult.setAlternatingRowBgColorAlgo1(ColorTranslator.FromHtml(GlobalUtils.tableRowColor1), ColorTranslator.FromHtml(GlobalUtils.tableRowColor2));
                    documentHTML.addSectionText(CreateReportSubHeader(@"<a name=""" + tcDataCarrier.Rows[i][GlobalUtils.tcTblTestScenario].ToString() + @""">" + tcDataCarrier.Rows[i][GlobalUtils.tcTblTestScenario].ToString() + "</a>"));
                    documentHTML.addSectionData(rsdTestCaseResult);
                    documentHTML.addSectionText(InsertNewLine());
                }
                #endregion

                #region HTML: Save HTML file
                streamWriter.WriteLine(documentHTML.getResult(true, true));
                streamWriter.Close();
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void GenerateOverallReport()
        {
            try
            {
                DocumentHTML documentHTML = new DocumentHTML();
                StreamWriter streamWriter = new StreamWriter(GlobalUtils.resultsOverallHtmlPath);

                #region HTML: Variables - Report Title
                string header = string.Format("[{0}] Overall Test Report", GlobalUtils.projectName);
                #endregion

                #region HTML: Variables - Test Module Result Table
                DataTable modDataTable = GetModResultData();
                DataView modDataView = new DataView(modDataTable);
                int modDataColumnCount = modDataTable.Columns.Count;
                int modDataResultColumn = modDataTable.Columns.IndexOf(GlobalUtils.modTblResult);
                int modDataTotalTCColumn = modDataTable.Columns.IndexOf(GlobalUtils.modTblTotalTC);
                int modDataPassedColumn = modDataTable.Columns.IndexOf(GlobalUtils.modTblPassed);
                int modDataFailedColumn = modDataTable.Columns.IndexOf(GlobalUtils.modTblFailed);
                int modDataPassRateColumn = modDataTable.Columns.IndexOf(GlobalUtils.modTblPassRate);
                int[] modNarrowCols = { modDataResultColumn, modDataTotalTCColumn, modDataPassedColumn, modDataFailedColumn, modDataPassRateColumn };
                #endregion

                #region HTML: Add report title
                documentHTML.addSectionText(CreateReportContent(GlobalUtils.htmlFormatter));
                documentHTML.addSectionText(CreateReportHeader(header));
                documentHTML.addSectionText(InsertNewLine());
                #endregion

                #region HTML: Add report summary
                documentHTML.addSectionText(CreateOverallReportSummary());
                documentHTML.addSectionText(InsertNewLine());
                #endregion

                #region HTML: Add Module Result table
                modDataView.Sort = GlobalUtils.modTblTestModule;
                ReportSectionData rsdTestScenarioResult = new ReportSectionData(modDataView.ToTable(), false);
                rsdTestScenarioResult.BgColorColumnTitle = SetColors(modDataColumnCount, GlobalUtils.tableHeaderBGColor);
                rsdTestScenarioResult.ForeColorTitle = SetColors(modDataColumnCount, GlobalUtils.tableHeaderTextColor);
                rsdTestScenarioResult.Alignment = ContentAlignment.MiddleCenter;
                rsdTestScenarioResult.Position = Position.center;
                rsdTestScenarioResult.TotalWidth = GlobalUtils.tableResultsWidth;
                rsdTestScenarioResult.HighLightColor = ColorTranslator.FromHtml(GlobalUtils.tableHighlightColor);
                rsdTestScenarioResult.ColWidth = SetColumnWidth(modDataColumnCount, modNarrowCols);
                rsdTestScenarioResult.setAlternatingRowBgColorAlgo1(ColorTranslator.FromHtml(GlobalUtils.tableRowColor1), ColorTranslator.FromHtml(GlobalUtils.tableRowColor2));
                documentHTML.addSectionData(rsdTestScenarioResult);
                documentHTML.addSectionText(InsertNewLine());
                documentHTML.addSectionText(InsertNewLine());
                documentHTML.addSectionText(InsertNewLine());
                #endregion

                #region HTML: Save HTML file
                streamWriter.WriteLine(documentHTML.getResult(true, true));
                streamWriter.Close();
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
                throw;
            }
        }

        public static void ZipResults(string directory)
        {
            try
            {
                string sourcePath = directory.Remove(directory.LastIndexOf("\\"));
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.FileName = GlobalUtils.resultsFile;
                saveFileDialog.DefaultExt = GlobalUtils.resultsZipExt;

                bool? showDlgResult = saveFileDialog.ShowDialog();

                if (showDlgResult.Equals(true))
                {
                    string destPath = saveFileDialog.FileName;
                    FileAdapter.DeleteFile(destPath);
                    FileAdapter.CreateZippedFiles(sourcePath, destPath);

                    MainWindow.MainAppWindow.LblTestMethod_SetText(GlobalUtils.lblTestMethodReportSuccess + destPath);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
        }

        public static string GetTestScenarioName(string callerFilePath)
        {
            string testScenario = null;
            string delimiter = "\\";

            try
            {
                if (callerFilePath.Contains(delimiter))
                {
                    testScenario = callerFilePath.Substring(callerFilePath.LastIndexOf(delimiter) + 1);
                    testScenario = testScenario.Remove(testScenario.LastIndexOf('.'));
                }
                else
                {
                    testScenario = callerFilePath;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
            return testScenario;
        }

        private static void UpdateGUIScriptsPanel(bool testCaseResult, string testCaseID, bool isBlocked = false)
        {
            System.Windows.Media.BrushConverter brushConverter = new System.Windows.Media.BrushConverter();
            if (isBlocked)
            {
                MainWindow.MainAppWindow.TblockScriptsViewerNum.Inlines.Add(new Run(string.Format("{0} {1}", GlobalUtils.iconBlocked, Environment.NewLine)) { Foreground = (System.Windows.Media.Brush)brushConverter.ConvertFromString(GlobalUtils.brushNewTest) });
            }
            else
            {
                if (testCaseResult)
                {
                    MainWindow.MainAppWindow.TblockScriptsViewerNum.Inlines.Add(new Bold(new Run(string.Format("{0} {1}", GlobalUtils.iconPassed, Environment.NewLine)) { Foreground = (System.Windows.Media.Brush)brushConverter.ConvertFromString(GlobalUtils.brushPassed) }));
                }
                else
                {
                    MainWindow.MainAppWindow.TblockScriptsViewerNum.Inlines.Add(new Bold(new Run(string.Format("{0} {1}", GlobalUtils.iconFailed, Environment.NewLine)) { Foreground = (System.Windows.Media.Brush)brushConverter.ConvertFromString(GlobalUtils.brushFailed) }));
                }
            }

            MainWindow.MainAppWindow.TblockScriptsViewer.Inlines.Add(testCaseID + Environment.NewLine);
            MainWindow.MainAppWindow.ScrollViewerTestScripts.ScrollToEnd();
        }

        private static string WriteResultsUtil(string screenShotType, bool testCaseResult, string testCaseID, string testDescription, string testScenario)
        {
            string content;
            string imgRelativePath = null;
            bool isBlocked = testCaseID.Contains(GlobalUtils.tableLabelTCBlocked);

            #region Content: Build content for results text file
            if (isBlocked)
            {
                testCaseID = testScenario + "_" + testCaseID;
            }
            else
            {
                testScenario = GetTestScenarioName(testScenario);
                testCaseID = testScenario + "_" + testCaseID;
            }

            switch (screenShotType)
            {
                case GlobalUtils.screenShotType1:
                    imgRelativePath = WebDriver.TakeScreenshot(testScenario);
                    break;
                case GlobalUtils.screenShotType2:
                    imgRelativePath = WebDriver.TakeHTMLScreenshot(testScenario);
                    break;
            }

            content = SetTextFileContent(testCaseResult, testScenario, testCaseID, testDescription, imgRelativePath);
            #endregion

            GlobalUtils.InterruptFlag.WaitOne();

            #region Text File: Write raw text results for HTML
            FileAdapter.AppendFile(GlobalUtils.resultsTxtPath, content);
            #endregion

            GenerateReport();
            return testCaseID;
        }

        private static string SetTextFileContent(bool testCaseResult, string testScenario, string testCaseID, string testDescription, string imgRelativePath, string stepResults = "")
        {
            string result = null;

            if (stepResults.Equals(""))
            {
                result = testCaseResult.ToString().Replace("True", "<a target='_blank' href=\"" + imgRelativePath + "\">" + GlobalUtils.tableLabelPassed + "</a>").Replace("False", "<a target='_blank' href=\"" + imgRelativePath + "\">" + GlobalUtils.tableLabelFailed + "</a>");
            }
            else
            {
                result = stepResults;
            }

            return string.Format("{0},{1},{2},{3},{4},{5}{6}",
            GlobalUtils.currentIter,
            testScenario,
            testCaseID,
            testDescription,
            result,
            DateTime.Now.ToString(),
            Environment.NewLine);
        }

        private static ReportSectionText CreateReportHeader(string content)
        {
            ReportSectionText reportSectionText = null;
            try
            {
                reportSectionText = new ReportSectionText("<table><tr><td class=\"header\">" + content + "</td></tr></table>");
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
            return reportSectionText;
        }

        private static ReportSectionText CreateReportSubHeader(string content)
        {
            ReportSectionText reportSectionText = null;
            try
            {
                reportSectionText = new ReportSectionText("<table><tr><td class=\"subheader\">" + content + "</td></tr></table>");
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
            return reportSectionText;
        }

        private static ReportSectionText CreateReportSummary(int passedCount, int failedCount)
        {
            ReportSectionText reportSectionText = null;
            try
            {
                string header = "<tr><td class=\"summary-header\">Summary: </td><td class=\"summary-data\"></td></tr>";
                string environment = "<tr><td class=\"summary-data\"><b>Environment:</b></td><td class=\"summary-data\">" + GlobalUtils.testEnvironment + "</td></tr>";
                string module = "<tr><td class=\"summary-data\"><b>Module:</b></td><td class=\"summary-data\">" + GlobalUtils.testModule + "</td></tr>";
                string total = "<tr><td class=\"summary-data\"><b>Total Executed:</b></td><td class=\"summary-data\">" + (passedCount + failedCount) + "</td></tr>";
                string passed = "<tr><td class=\"summary-data\"><b>" + GlobalUtils.tableLabelPassed + ":</b></td><td class=\"summary-data\">" + passedCount + "</td></tr>";
                string failed = "<tr><td class=\"summary-data\"><b>" + GlobalUtils.tableLabelFailed + ":</b></td><td class=\"summary-data\">" + failedCount + "</td></tr>";
                string summary = string.Format("<table class=\"summary\">{0}{1}{2}{3}{4}{5}</table>", header, module, environment, total, passed, failed);

                reportSectionText = new ReportSectionText(summary);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
            return reportSectionText;
        }

        private static ReportSectionText CreateOverallReportSummary()
        {
            ReportSectionText reportSectionText = null;
            try
            {
                string header = "<tr><td class=\"summary-header\">Summary: </td><td class=\"summary-data\"></td></tr>";
                string date = "<tr><td class=\"summary-data\"><b>Date:</b></td><td class=\"summary-data\">" + dateOverall + "</td></tr>";
                string environment = "<tr><td class=\"summary-data\"><b>Environment:</b></td><td class=\"summary-data\">" + environmentOverall + "</td></tr>";
                string total = "<tr><td class=\"summary-data\"><b>Total Executed:</b></td><td class=\"summary-data\">" + totalExecOverall + "</td></tr>";
                string passed = "<tr><td class=\"summary-data\"><b>" + GlobalUtils.tableLabelPassed + ":</b></td><td class=\"summary-data\">" + passedCountOverall + "</td></tr>";
                string failed = "<tr><td class=\"summary-data\"><b>" + GlobalUtils.tableLabelFailed + ":</b></td><td class=\"summary-data\">" + failedCountOverall + "</td></tr>";
                string passedRate = "<tr><td class=\"summary-data\"><b>" + GlobalUtils.tableLabelPassedRate + ":</b></td><td class=\"summary-data\">" + (int)passedRateOverall + "%" + "</td></tr>";
                string summary = string.Format("<table class=\"summary\">{0}{1}{2}{3}{4}{5}{6}</table>", header, date, environment, total, passed, failed, passedRate);

                reportSectionText = new ReportSectionText(summary);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
            return reportSectionText;
        }

        private static ReportSectionText CreateReportContent(string content)
        {
            ReportSectionText reportSectionText = null;
            try
            {
                reportSectionText = new ReportSectionText(content);
                reportSectionText.FontText = new Font(GlobalUtils.fontContent, 14.0f, System.Drawing.FontStyle.Regular);
                reportSectionText.ForeColor = ColorTranslator.FromHtml(GlobalUtils.colorContent);
                reportSectionText.Alignment = ContentAlignment.MiddleLeft;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
            return reportSectionText;
        }

        private static ReportSectionText InsertNewLine()
        {
            ReportSectionText reportSectionText = null;
            try
            {
                reportSectionText = new ReportSectionText("<br/>");
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
            return reportSectionText;
        }

        private static DataTable GetModResultData()
        {
            DataTable dataTable = new DataTable();

            try
            {
                string result;
                float passedCount;
                float failedCount;
                float totalExec;
                float passedRate;

                environmentOverall = "";
                dateOverall = "";
                passedCountOverall = 0;
                failedCountOverall = 0;
                totalExecOverall = 0;
                passedRateOverall = 0;

                dataTable.Columns.Add(GlobalUtils.modTblTestModule);
                dataTable.Columns.Add(GlobalUtils.modTblResult);
                dataTable.Columns.Add(GlobalUtils.modTblTotalTC);
                dataTable.Columns.Add(GlobalUtils.modTblPassed);
                dataTable.Columns.Add(GlobalUtils.modTblFailed);
                dataTable.Columns.Add(GlobalUtils.modTblPassRate);

                DirectoryInfo dir = new DirectoryInfo(GlobalUtils.resultsOverallPath);
                DirectoryInfo[] dirs = dir.GetDirectories();

                #region DataTable: Build row data for each module
                foreach (DirectoryInfo mod in dirs)
                {
                    string resultsTxtPath = mod.FullName + "\\" + GlobalUtils.resultsTextFile;

                    DataRow dataRow = dataTable.NewRow();
                    result = null;
                    passedCount = 0;
                    failedCount = 0;
                    passedRate = 0;

                    #region Get passed and failed count for each test module
                    passedCount = GetModuleResultSummary(resultsTxtPath)[GlobalUtils.tableLabelPassed];
                    failedCount = GetModuleResultSummary(resultsTxtPath)[GlobalUtils.tableLabelFailed];
                    totalExec = passedCount + failedCount;

                    if (totalExec > 0)
                    {
                        passedRate = (passedCount / totalExec) * 100;
                    }
                    else
                    {
                        passedRate = 0;
                    }

                    if (failedCount > 0)
                    {
                        result = GlobalUtils.tableLabelFailed;
                    }
                    else
                    {
                        result = GlobalUtils.tableLabelPassed;
                    }
                    #endregion

                    dataRow[0] = mod;
                    dataRow[1] = result.Replace(GlobalUtils.tableLabelPassed, "<a href = \"" + mod + "\\" + GlobalUtils.resultsHtmlFile + "\"><font color=" + "\"" + GlobalUtils.brushPassed + "\"" + ">" + GlobalUtils.tableLabelPassed + "</font></a>").Replace(GlobalUtils.tableLabelFailed, "<a href = \"" + mod + "\\" + GlobalUtils.resultsHtmlFile + "\"><font color=" + "\"" + GlobalUtils.brushFailed + "\"" + ">" + GlobalUtils.tableLabelFailed + "</font></a>");
                    dataRow[2] = totalExec;
                    dataRow[3] = passedCount;
                    dataRow[4] = failedCount;
                    dataRow[5] = (int)passedRate + "%";
                    dataTable.Rows.Add(dataRow);

                    #region Calculate overall passed and failed count
                    environmentOverall = mod.Parent.ToString();
                    dateOverall = mod.Parent.Parent.ToString();
                    passedCountOverall = passedCountOverall + passedCount;
                    failedCountOverall = failedCountOverall + failedCount;
                    totalExecOverall = totalExecOverall + totalExec;
                    #endregion
                }

                #region Calculate overall passed rate
                if (totalExecOverall > 0)
                {
                    passedRateOverall = (passedCountOverall / totalExecOverall) * 100;
                }
                else
                {
                    passedRateOverall = 0;
                }
                #endregion

                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
            return dataTable;
        }

        private static DataTable GetTSResultData()
        {
            DataTable dataTable = new DataTable();

            try
            {
                List<string> testScenarioList = new List<string>();
                string result;
                float passedCount;
                float failedCount;
                float totalExec;
                float passedRate;

                dataTable.Columns.Add(GlobalUtils.tsTblTestScenario);
                dataTable.Columns.Add(GlobalUtils.tsTblResult);
                dataTable.Columns.Add(GlobalUtils.tsTblTotalTC);
                dataTable.Columns.Add(GlobalUtils.tsTblPassed);
                dataTable.Columns.Add(GlobalUtils.tsTblFailed);
                dataTable.Columns.Add(GlobalUtils.tsTblPassRate);

                #region List: Get all test scenarios
                testScenarioList = GetTestScenarioList();
                #endregion

                #region DataTable: Build row data
                foreach (string testScenario in testScenarioList)
                {
                    DataRow dataRow = dataTable.NewRow();
                    result = null;
                    passedCount = 0;
                    failedCount = 0;
                    passedRate = 0;

                    #region Get passed and failed count for each test scenario
                    passedCount = GetTestScenarioResult(testScenario)[GlobalUtils.tableLabelPassed];
                    failedCount = GetTestScenarioResult(testScenario)[GlobalUtils.tableLabelFailed];
                    totalExec = passedCount + failedCount;

                    if (totalExec > 0)
                    {
                        passedRate = (passedCount / totalExec) * 100;
                    }
                    else
                    {
                        passedRate = 0;
                    }

                    if (failedCount > 0)
                    {
                        result = GlobalUtils.tableLabelFailed;
                    }
                    else
                    {
                        result = GlobalUtils.tableLabelPassed;
                    }
                    #endregion

                    dataRow[0] = testScenario;
                    dataRow[1] = result.Replace(GlobalUtils.tableLabelPassed, "<a href = \"#" + testScenario + "\"><font color=" + "\"" + GlobalUtils.brushPassed + "\"" + ">" + GlobalUtils.tableLabelPassed + "</font></a>").Replace(GlobalUtils.tableLabelFailed, "<a href = \"#" + testScenario + "\"><font color=" + "\"" + GlobalUtils.brushFailed + "\"" + ">" + GlobalUtils.tableLabelFailed + "</font></a>");
                    dataRow[2] = totalExec;
                    dataRow[3] = passedCount;
                    dataRow[4] = failedCount;
                    dataRow[5] = (int)passedRate + "%";
                    dataTable.Rows.Add(dataRow);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
            return dataTable;
        }

        private static DataTable GetTCResultData()
        {
            DataTable dataTable = new DataTable();

            try
            {
                dataTable.Columns.Add(GlobalUtils.tcTblIteration);
                dataTable.Columns.Add(GlobalUtils.tcTblTestScenario);
                dataTable.Columns.Add(GlobalUtils.tcTblTestCase);
                dataTable.Columns.Add(GlobalUtils.tcTblDescription);
                dataTable.Columns.Add(GlobalUtils.tcTblResult);
                dataTable.Columns.Add(GlobalUtils.tcTblTimeStamp);

                foreach (string line in File.ReadLines(GlobalUtils.resultsTxtPath))
                {
                    DataRow dataRow = dataTable.NewRow();
                    string[] cols = line.Split(GlobalUtils.resultsFileDelimiter);

                    for (int foo = 0; foo <= cols.Length - 1; foo++)
                    {
                        dataRow[foo] = cols[foo].Replace(GlobalUtils.tableLabelPassed, "<font color=" + "\"" + GlobalUtils.brushPassed + "\"" + ">" + GlobalUtils.tableLabelPassed + "</font>").Replace(GlobalUtils.tableLabelFailed, "<font color=" + "\"" + GlobalUtils.brushFailed + "\"" + ">" + GlobalUtils.tableLabelFailed + "</font>");
                    }
                    dataTable.Rows.Add(dataRow);
                }

                if (!Convert.ToBoolean(GlobalUtils.loopedRun))
                {
                    dataTable.Columns.RemoveAt(0);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }

            #region Sorted DataTable
            DataView dataView = dataTable.DefaultView;
            dataView.Sort = GlobalUtils.tcTblTestScenario;
            DataTable sortedDataTable = dataView.ToTable();
            #endregion

            return dataTable;
        }

        private static List<string> GetTestScenarioList()
        {
            List<string> testScenarioList = new List<string>();

            try
            {
                foreach (string line in File.ReadLines(GlobalUtils.resultsTxtPath))
                {
                    string[] cols = line.Split(GlobalUtils.resultsFileDelimiter);
                    testScenarioList.Add(cols[1]);
                    testScenarioList = testScenarioList.Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
            testScenarioList.Sort();
            return testScenarioList;
        }

        private static Dictionary<string, int> GetModuleResultSummary(string resultsTxtPath = "")
        {
            int passedCount = 0;
            int failedCount = 0;
            Dictionary<string, int> modResultSummary = new Dictionary<string, int>();

            if (resultsTxtPath.Equals(""))
            {
                resultsTxtPath = GlobalUtils.resultsTxtPath;
            }

            try
            {
                foreach (string line in File.ReadLines(resultsTxtPath))
                {
                    if (line.Contains(GlobalUtils.tableLabelFailed))
                    {
                        failedCount++;
                    }
                    else if (line.Contains(GlobalUtils.tableLabelPassed))
                    {
                        passedCount++;
                    }
                }
                modResultSummary.Add(GlobalUtils.tableLabelPassed, passedCount);
                modResultSummary.Add(GlobalUtils.tableLabelFailed, failedCount);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
            return modResultSummary;
        }

        private static Dictionary<string, int> GetTestScenarioResult(string testScenario)
        {
            int passedCount = 0;
            int failedCount = 0;
            Dictionary<string, int> tsResult = new Dictionary<string, int>();

            try
            {
                foreach (string line in File.ReadLines(GlobalUtils.resultsTxtPath))
                {
                    string[] cols = line.Split(GlobalUtils.resultsFileDelimiter);

                    if (line.Contains(GlobalUtils.tableLabelFailed) && cols[1].Equals(testScenario))
                    {
                        failedCount++;
                    }
                    else if (line.Contains(GlobalUtils.tableLabelPassed) && cols[1].Equals(testScenario))
                    {
                        passedCount++;
                    }
                }
                tsResult.Add(GlobalUtils.tableLabelPassed, passedCount);
                tsResult.Add(GlobalUtils.tableLabelFailed, failedCount);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
            return tsResult;
        }

        private static bool GetTestResult(bool[] testStepResults)
        {
            bool testResult = true;

            try
            {
                foreach (bool result in testStepResults)
                {
                    testResult = testResult && result;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }
            return testResult;
        }

        private static Color[] SetColors(int arraySize, string hexColor)
        {
            List<Color> colors = new List<Color>();

            try
            {
                for (int foo = 0; foo < arraySize; foo++)
                {
                    colors.Add(ColorTranslator.FromHtml(hexColor));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }

            return colors.ToArray();
        }

        private static int[] SetColumnWidth(int arraySize, int[] narrowCols = null, int[] wideCols = null)
        {
            List<int> columnWidth = new List<int>();
            int split = GlobalUtils.tableResultsWidth / arraySize;
            narrowCols = narrowCols ?? new int[0];
            wideCols = wideCols ?? new int[0];

            try
            {
                for (int foo = 0; foo < arraySize; foo++)
                {
                    if (narrowCols.Contains(foo))
                    {
                        columnWidth.Add(split / 2);
                    }
                    else if (wideCols.Contains(foo))
                    {
                        columnWidth.Add(split * 2);
                    }
                    else
                    {
                        columnWidth.Add(split);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("{0} Exception:", GlobalUtils.GetCurrentMethodName()), ex);
            }

            return columnWidth.ToArray();
        }
        //========================================================================REPORT UTILITIES====================================================================== 
    }
}
